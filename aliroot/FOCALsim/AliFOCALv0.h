#ifndef ALIFOCALV0_H
#define ALIFOCALV0_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

/* $Id: AliFOCALv0.h 8621 2003-11-12 06:29:14Z bnandi $ */

////////////////////////////////////////////////
//  Manager and hits classes for set:FOCAL      //
////////////////////////////////////////////////
 
#include "AliFOCAL.h"
#include "AliFOCALGeometry.h"
#include "TGeoManager.h"

//___________________________________________


class AliFOCALGeomtory;
 
class AliFOCALv0 : public AliFOCAL {

public:
  AliFOCALv0();
  AliFOCALv0(const char *name, const char *title);
  AliFOCALv0(const char *name, const char *title, const char * gfile);
  AliFOCALv0 &operator=(const AliFOCALv0 &ali);
  
  virtual      ~AliFOCALv0() ;
  virtual void  CreateGeometry();
  virtual void  CreateFOCAL();
  virtual void  CreateSupermodule();
  virtual void  CreateMaterials();
  virtual void  Init();
  virtual Int_t IsVersion() const {return 1;}
  virtual void  StepManager();
  
  void SetStoreFullShower(Bool_t flag) {fStoreFullShower = flag;}
  virtual void  DrawModule() const;
  virtual void   AddAlignableVolumes() const;      // define sym.names for alignable volumes
  virtual void FinishPrimary();
  
 private:

  TObjArray *fGeometryObject;
  TString    fGeometryFile;
  Int_t  fMedSens;           // Sensitive Medium (Si)
  Int_t  fMedSensHCal;       // Sensitive Medium for HCal
  Int_t  fStoreFullShower;   // Flag to store full shower history (uses a lot of disk space an memery; not recommended)
  Int_t  fCurrentTrack;      //! Current track in StepManager
  Int_t  fCurrentMother;     //! Current morther track (track hitting FOCAL) in StepManager

  ClassDef(AliFOCALv0,4)  //Hits manager for set:FOCAL
};
 
#endif


