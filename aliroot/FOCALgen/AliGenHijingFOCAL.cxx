/**************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

/* $Id$ */

// Generator using HIJING as an external generator
// The main HIJING options are accessable for the user through this interface.
// Uses the THijing implementation of TGenerator.
// Author:
// Andreas Morsch    (andreas.morsch@cern.ch)
//

#include <TClonesArray.h>
#include <TGraph.h>
#include <THijing.h>
#include <TLorentzVector.h>
#include <TPDGCode.h>
#include <TParticle.h>

#include "AliGenHijingFOCAL.h"
#include "AliGenHijingEventHeader.h"
#include "AliHijingRndm.h"
#include "AliLog.h"
#include "AliRun.h"

ClassImp(AliGenHijingFOCAL)

void AliGenHijingFOCAL::Init()
{
  // Initialisation
  fFrame.Resize(8);
  fTarget.Resize(8);
  fProjectile.Resize(8);
  
  SetMC(new THijing(fEnergyCMS, fFrame, fProjectile, fTarget, 
		    fAProjectile, fZProjectile, fATarget, fZTarget, 
		    fMinImpactParam, fMaxImpactParam));

  fHijing=(THijing*) fMCEvGen;
  fHijing->SetIHPR2(2,  fRadiation);
  if (fTrigger == kHardProcesses || fTrigger == kDirectPhotons)
    fHijing->SetIHPR2(3,  fTrigger);
  fHijing->SetIHPR2(6,  fShadowing);
  fHijing->SetIHPR2(12, fDecaysOff);    
  fHijing->SetIHPR2(21, fKeep);
  fHijing->SetHIPR1(8,  fPtHardMin); 	
  fHijing->SetHIPR1(9,  fPtHardMax); 	
  if (fTrigger == kHardProcesses || fTrigger == kDirectPhotons)
    fHijing->SetHIPR1(10, fPtMinJet); 
  if (fSigmaNN>0)
    fHijing->SetHIPR1(31, fSigmaNN/2.); 	
  fHijing->SetHIPR1(50, fSimpleJet);
  //
  // Switching off elastic scattering
  if (fNoElas)
    fHijing->SetIHPR2(14, 0);

  //
  //  Quenching
  //
  //
  //  fQuench = 0:  no quenching
  //  fQuench = 1:  hijing default
  //  fQuench = 2:  new LHC  parameters for HIPR1(11) and HIPR1(14)
  //  fQuench = 3:  new RHIC parameters for HIPR1(11) and HIPR1(14)
  //  fQuench = 4:  new LHC  parameters with log(e) dependence
  //  fQuench = 5:  new RHIC parameters with log(e) dependence
  fHijing->SetIHPR2(50, 0);
  if (fQuench > 0) 
    fHijing->SetIHPR2(4,  1);
  else
    fHijing->SetIHPR2(4,  0);
  // New LHC parameters from Xin-Nian Wang
  if (fQuench == 2) {
    fHijing->SetHIPR1(14, 1.1);
    fHijing->SetHIPR1(11, 3.7);
  } else if (fQuench == 3) {
    fHijing->SetHIPR1(14, 0.20);
    fHijing->SetHIPR1(11, 2.5);
  } else if (fQuench == 4) {
    fHijing->SetIHPR2(50, 1);
    fHijing->SetHIPR1(14, 4.*0.34);
    fHijing->SetHIPR1(11, 3.7);
  } else if (fQuench == 5) {
    fHijing->SetIHPR2(50, 1);
    fHijing->SetHIPR1(14, 0.34);
    fHijing->SetHIPR1(11, 2.5);
  }
    
  //
  // Heavy quarks
  //    
  if (fNoHeavyQuarks) {
    fHijing->SetIHPR2(49, 1);
  } else {
    fHijing->SetIHPR2(49, 0);
  }
    
  AliGenMC::Init();
    
  //
  //  Initialize Hijing  
  //    
  fHijing->Initialize();
  //
  if (fEvaluate) 
    EvaluateCrossSections();
}

Bool_t AliGenHijingFOCAL::CheckTrigger()
{
  // Check the kinematic trigger condition
  //
  Bool_t   triggered = kFALSE;
  if (fTrigger == kHardProcesses) {
    //  jet-jet Trigger	
    TLorentzVector* jet1 = new TLorentzVector(fHijing->GetHINT1(26), 
					      fHijing->GetHINT1(27),
					      fHijing->GetHINT1(28),
					      fHijing->GetHINT1(29));
	
    TLorentzVector* jet2 = new TLorentzVector(fHijing->GetHINT1(36), 
					      fHijing->GetHINT1(37),
					      fHijing->GetHINT1(38),
					      fHijing->GetHINT1(39));
    Double_t eta1      = jet1->Eta();
    Double_t eta2      = jet2->Eta();
    Double_t phi1      = jet1->Phi();
    Double_t phi2      = jet2->Phi();
    //printf("\n Trigger: %f %f %f %f", fEtaMinJet, fEtaMaxJet, fPhiMinJet, fPhiMaxJet);
    if (
	(eta1 < fEtaMaxJet && eta1 > fEtaMinJet &&  
	 phi1 < fPhiMaxJet && phi1 > fPhiMinJet) 
	||
	(eta2 < fEtaMaxJet && eta2 > fEtaMinJet &&  
	 phi2 < fPhiMaxJet && phi2 > fPhiMinJet)
	) 
      triggered = kTRUE;
  } else if (fTrigger == kDirectPhotons) {
    //  Gamma Jet
    Int_t np = fParticles.GetEntriesFast();
    for (Int_t i = 0; i < np; i++) {
      TParticle* part = (TParticle*) fParticles.At(i);
      Int_t kf = part->GetPdgCode();
      Int_t ksp = part->GetUniqueID();
      if (kf == 22 && ksp == 40) {
	Float_t phi = part->Phi();
	Float_t eta = part->Eta();
	if  (eta < fEtaMaxJet && 
	     eta > fEtaMinJet &&
	     phi < fPhiMaxJet && 
	     phi > fPhiMinJet &&
	     part->Pt() > fPtMinJet) {
	  triggered = 1;
	  break;
	} // check phi,eta within limits
      } // direct gamma ? 
    } // particle loop
  } else if (fTrigger == kDecayPhotons) {
    //  Decay photon
    Int_t np = fParticles.GetEntriesFast();
    for (Int_t i = 0; i < np; i++) {
      TParticle* part = (TParticle*) fParticles.At(i);
      Int_t kf = part->GetPdgCode();
      Int_t ksp = part->GetUniqueID();
      if (kf == 22 || TMath::Abs(kf)==11) {  //photon or electron
	Float_t phi = part->Phi();
	Float_t eta = part->Eta();
	if  (eta < fEtaMaxJet && 
	     eta > fEtaMinJet &&
	     phi < fPhiMaxJet && 
	     phi > fPhiMinJet &&
	     part->Pt() > fPtMinJet) {
	  triggered = 1;
	  break;
	} // check phi,eta within limits
      } // direct gamma ? 
    } // particle loop
  } // fTrigger == 3
  return triggered;
}
