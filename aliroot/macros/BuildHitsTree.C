//
// Macro that extracts hits from a list of FOCAL simulation directories and 
//   creates a flat tree containing the coordinates of all hits and info on the 
//   primary MC particles that created the hit
// contact: Ionut Arsene (iarsene@cern.ch)

#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include "AliFOCALGeometry.h"
#include "AliFOCALdigit.h"
#include "AliFOCALDigitizer.h"
#include "AliRunLoader.h"
#include "AliFOCALLoader.h"
#include "AliFOCAL.h"
#include "AliFOCALhit.h"
#include "AliRun.h"
#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

char gGeomFilename[200] = "geometry.txt"; // geometry filename

TTree* gHitsTree;
AliFOCALGeometry* gFocalGeometry;
int gEventNumber;
int gEventCounter;
short row, col, layer, seg, towerX, towerY, padWaferX, padWaferY, sensorX, sensorY;
float x, y, z, depEn;
int particleNumber, pdgCode, particleNumber_ancestor1, pdgCode_ancestor1;
float px, py, pz, px_ancestor, py_ancestor, pz_ancestor;
short isValid;

void ProcessFile(TString filename, int howMany=-1, int offset=0, bool isGrid=false);
void BuildHitsTree();
void BuildHitsTree(const char* inputFilename, const char* outputFilename="HitsTree.root", int howMany=-1, int offset=0);

//______________________________________________________________________________________
Bool_t LoadLibs() {
  if(!gSystem->Getenv("FOCAL")) {
    cout << "ERROR: FOCAL environment not enabled! Use alienv to load it!" << endl;
    return kFALSE;
  }
  gSystem->Load("libpythia6_4_28.so");
  gSystem->Load("libFOCALbase.so");
  gSystem->Load("libFOCALsim.so");
  gSystem->Load("libFOCALgen.so");
  return kTRUE;
}

//______________________________________________________________________________________
void BuildHitsTree(const char* inputFilename, const char* outputFilename/*="HitsTree.root"*/, int howMany/*=-1*/, int offset/*=0*/) {
  //
  // inputFilename: text file containing the list of simulation directories to be processed
  //                each directory must contain at least the galice.root, Kinematics and FOCAL hits files
  // howMany: the number of events to be processed
  // offset:  leave the first "offset" events unprocessed
        
  LoadLibs();
    
  TFile* save = new TFile(outputFilename, "RECREATE");
  gHitsTree = new TTree("hitsTree","Hits tree");
  gHitsTree->Branch("iEvent",&gEventNumber,"Event/I");
  gHitsTree->Branch("row",&row,"row/S");
  gHitsTree->Branch("col",&col,"col/S");
  gHitsTree->Branch("layer",&layer,"layer/S");
  gHitsTree->Branch("seg",&seg,"seg/S");
  gHitsTree->Branch("towerX",&towerX,"towerX/S");
  gHitsTree->Branch("towerY",&towerY,"towerY/S");
  gHitsTree->Branch("padWaferX",&padWaferX,"padWaferX/S");
  gHitsTree->Branch("padWaferY",&padWaferY,"padWaferY/S");
  gHitsTree->Branch("sensorX",&sensorX,"sensorX/S");
  gHitsTree->Branch("sensorY",&sensorY,"sensorY/S");
  gHitsTree->Branch("x",&x,"x/F");          // cm
  gHitsTree->Branch("y",&y,"y/F");          // cm
  gHitsTree->Branch("z",&z,"z/F");          // cm
  gHitsTree->Branch("depEn",&depEn,"depEn/F");   // eV
  gHitsTree->Branch("particleNumber",&particleNumber,"particleNumber/I");
  gHitsTree->Branch("pdgCode",&pdgCode,"pdgCode/I");
  gHitsTree->Branch("particleNumber_ancestor1",&particleNumber_ancestor1,"particleNumber_ancestor1/I");
  gHitsTree->Branch("pdgCode_ancestor1",&pdgCode_ancestor1,"pdgCode_ancestor1/I");
  gHitsTree->Branch("px",&px,"px/F");    // GeV/c
  gHitsTree->Branch("py",&py,"py/F");    // GeV/c 
  gHitsTree->Branch("pz",&pz,"pz/F");    // GeV/c
  gHitsTree->Branch("px_ancestor",&px_ancestor,"px_ancestor/F");    // GeV/c
  gHitsTree->Branch("py_ancestor",&py_ancestor,"py_ancestor/F");    // GeV/c 
  gHitsTree->Branch("pz_ancestor",&pz_ancestor,"pz_ancestor/F");    // GeV/c
  gHitsTree->Branch("isValid", &isValid, "isValid/S");  // whether geometry info is valid
  
  // Get geometry
  gFocalGeometry = new AliFOCALGeometry();
  gFocalGeometry->Init(gGeomFilename);
    
  std::ifstream in;
  in.open(inputFilename);
                                                                                                                                                               
  TString line;
  //loop over all files in the input list
  gEventNumber = 0;
  gEventCounter = 0;
  while(in.good()) {
    in >> line;
    if (!line.IsNull()) {
      cout << "Processing directory " << line <<endl;
      ProcessFile(line, howMany, offset);
    }
    if (howMany>0 && gEventCounter>howMany) {
      break;        
    } 
  }
  
  save->cd();
  gHitsTree->Write();
  save->Close();
}

//______________________________________________________________________________________
void BuildHitsTree() {
  //
  // Function with no arguments. Assume that the input file is in the current directory 
  //
        
  LoadLibs();
    
  TFile* save = new TFile("HitsTree.root", "RECREATE");
  gHitsTree = new TTree("hitsTree","Hits tree");
  gHitsTree->Branch("iEvent",&gEventNumber,"Event/I");
  gHitsTree->Branch("row",&row,"row/S");
  gHitsTree->Branch("col",&col,"col/S");
  gHitsTree->Branch("layer",&layer,"layer/S");
  gHitsTree->Branch("seg",&seg,"seg/S");
  gHitsTree->Branch("towerX",&towerX,"towerX/S");
  gHitsTree->Branch("towerY",&towerY,"towerY/S");
  gHitsTree->Branch("padWaferX",&padWaferX,"padWaferX/S");
  gHitsTree->Branch("padWaferY",&padWaferY,"padWaferY/S");
  gHitsTree->Branch("sensorX",&sensorX,"sensorX/S");
  gHitsTree->Branch("sensorY",&sensorY,"sensorY/S");
  gHitsTree->Branch("x",&x,"x/F");          // cm
  gHitsTree->Branch("y",&y,"y/F");          // cm
  gHitsTree->Branch("z",&z,"z/F");          // cm
  gHitsTree->Branch("depEn",&depEn,"depEn/F");   // eV
  gHitsTree->Branch("particleNumber",&particleNumber,"particleNumber/I");
  gHitsTree->Branch("pdgCode",&pdgCode,"pdgCode/I");
  gHitsTree->Branch("particleNumber_ancestor1",&particleNumber_ancestor1,"particleNumber_ancestor1/I");
  gHitsTree->Branch("pdgCode_ancestor1",&pdgCode_ancestor1,"pdgCode_ancestor1/I");
  gHitsTree->Branch("px",&px,"px/F");    // GeV/c
  gHitsTree->Branch("py",&py,"py/F");    // GeV/c 
  gHitsTree->Branch("pz",&pz,"pz/F");    // GeV/c
  gHitsTree->Branch("px_ancestor",&px_ancestor,"px_ancestor/F");    // GeV/c
  gHitsTree->Branch("py_ancestor",&py_ancestor,"py_ancestor/F");    // GeV/c 
  gHitsTree->Branch("pz_ancestor",&pz_ancestor,"pz_ancestor/F");    // GeV/c
  gHitsTree->Branch("isValid", &isValid, "isValid/S");  // whether geometry info is valid
  
  // Get geometry
  gFocalGeometry = new AliFOCALGeometry();
  gFocalGeometry->Init(gGeomFilename);
    
  ProcessFile(".",-1,0,true);
  
  save->cd();
  gHitsTree->Write();
  save->Close();
}

//___________________________________________________________________________________________________________
void ProcessFile(TString dirName, int howMany/*=-1*/, int offset /*=0*/, bool isGrid /*=false*/) {
  
  // check the status of the input file
  long int id, size, flags, mt;
  if (gSystem->GetPathInfo(Form("%s/%s", dirName.Data(), (isGrid ? "root_archive.zip" : "galice.root")),&id,&size,&flags,&mt) == 1) {
    cout << "ERROR: galice.root not found in " << dirName.Data() << endl;
    return;
  }
    
  //Alice run loader
  AliRunLoader *runLoader = AliRunLoader::Open(Form("%s/%s", dirName.Data(), (isGrid ? "root_archive.zip#galice.root" : "galice.root")));
  if (!runLoader) {
    cout << "ERROR: run loader not initialized for " << dirName.Data() << endl;
    return;
  }
  if (!runLoader->GetAliRun()) runLoader->LoadgAlice();
  if (!runLoader->TreeE()) runLoader->LoadHeader();
  if (!runLoader->TreeK()) runLoader->LoadKinematics();
  gAlice = runLoader->GetAliRun();
        
  //Initialize AliFOCAL and focal loader
  AliFOCAL *focal  = (AliFOCAL*)gAlice->GetDetector("FOCAL");
  AliFOCALLoader *focalLoader = dynamic_cast<AliFOCALLoader*>(runLoader->GetLoader("FOCALLoader"));
  focalLoader->LoadHits("READ");
  
  // Loop over events
  for (int iEvent = 0; iEvent < runLoader->GetNumberOfEvents(); iEvent++) {
     
    if (howMany>0 && gEventCounter>howMany) {
      return;        
    }
            
    int ie = runLoader->GetEvent(iEvent);
    if (ie != 0) {
      cout << "Error reading event " << iEvent << endl;
      continue;
    }
    if (gEventNumber<offset) {
      gEventNumber++;
      continue;
    }
            
    // get the hits tree and set the FOCAL branch
    TTree* treeH = focalLoader->TreeH();
    TClonesArray* hits = 0;
    treeH->SetBranchAddress("FOCAL", &hits);
    
    cout << "Processing Event " << iEvent << "/" << runLoader->GetNumberOfEvents() << " with " << treeH->GetEntries() << " tracks." << endl;
    
    // get the MC particle stack
    AliStack *stack = runLoader->Stack();
      
    for (int itrack=0; itrack < treeH->GetEntries(); itrack++) {
      treeH->GetEntry(itrack);
      int oldParticleNumber = -1;
      bool isNewParticle = true;
      // loop over hits for this track
      for(int ihit=0; ihit<hits->GetEntries(); ihit++) {  
        AliFOCALhit *hit = (AliFOCALhit*) hits->UncheckedAt(ihit);
        x = hit->X();
        y = hit->Y();
        z = hit->Z();
        depEn = hit->GetEnergy();

        // Find the corresponding column,row,layer and virtual segment using the virtual geometry
        // NOTE: (row,col) is a grid which does not take into acount empty spaces between sensitive areas
        int colTemp,rowTemp,segTemp,layerTemp;
        isValid = (gFocalGeometry->GetVirtualInfo(x,y,z,colTemp,rowTemp,layerTemp,segTemp) ? 1 : 0);
        row = static_cast<short>(rowTemp);
        col = static_cast<short>(colTemp);
        layer = static_cast<short>(layerTemp);
        seg = static_cast<short>(segTemp);
        
        int vol0 = hit->GetVolume(0);
        int vol1 = hit->GetVolume(1);
        int vol2 = hit->GetVolume(2);
        int vol3 = hit->GetVolume(3);
        
        towerX = static_cast<short>((vol1-1)%2);
        towerY = static_cast<short>((vol1-1)/2);
                
        // NOTE: wafer and sensor identification using the encoded information from the vol0 ID
        int sensorYtemp, sensorXtemp, stackDet, waferXtemp, waferYtemp;
        gFocalGeometry->GetPadPositionId2RowColStackLayer(vol0, sensorYtemp, sensorXtemp, stackDet, layerTemp, segTemp, waferXtemp, waferYtemp);
        
        sensorX = static_cast<short>(sensorXtemp);
        sensorY = static_cast<short>(sensorYtemp);
        padWaferX = static_cast<short>(waferXtemp);
        padWaferY = static_cast<short>(waferYtemp);
          
        // Identify the primary particle responsible for this hit.
        // If the hit->GetTrack() is not a primary particle, navigate into the particle stack until 
        //   one finds the nearest ancestor which is a physical primary.
        // The PDG code and momentum vector of that ancestor is then stored together with the hit
        particleNumber = hit->GetTrack();
        if (particleNumber != oldParticleNumber) {
          oldParticleNumber = particleNumber;
          isNewParticle = true;
        } else {
          isNewParticle = false;
        }

        if (isNewParticle) {
          TParticle *particle = stack->Particle(particleNumber);
          bool isPhysicalPrimary = stack->IsPhysicalPrimary(particleNumber);
        
          while (!isPhysicalPrimary) {
            particleNumber = particle->GetFirstMother();
            if (particleNumber>=0) {
              particle = stack->Particle(particleNumber);        
            } else {
              particle = 0x0;         
            }
            if (!particle) {
              cout << "primary particle not found" << endl;      
              break;        
            }
            isPhysicalPrimary = stack->IsPhysicalPrimary(particleNumber);
          }
         
          if(particle) {
            pdgCode = particle->GetPdgCode();
            px = particle->Px();
            py = particle->Py();
            pz = particle->Pz();
            particleNumber_ancestor1 = particle->GetFirstMother();
            TParticle* ancestor = 0x0;
            if(particleNumber_ancestor1>=0) {
              ancestor = stack->Particle(particleNumber_ancestor1);
            }
            
            if (ancestor) {
              pdgCode_ancestor1 = ancestor->GetPdgCode();
              px_ancestor = ancestor->Px();
              py_ancestor = ancestor->Py();
              pz_ancestor = ancestor->Pz();
            } else {
              pdgCode_ancestor1 = -9999;
              particleNumber_ancestor1 = -9999;
              px_ancestor = 0.0;
              py_ancestor = 0.0;
              pz_ancestor = 0.0;
            }
          }
          else {
            particleNumber = 0;      
            pdgCode = 0;
            pdgCode_ancestor1 = -9999;
            particleNumber_ancestor1 = -9999;
            px = 0;
            py = 0;
            pz = 0;
            px_ancestor = 0.0;
            py_ancestor = 0.0;
            pz_ancestor = 0.0;
          }
        }  // end if(isNewParticle)
        gHitsTree->Fill();          
      }  // end loop over hits in a track
    }  // end loop over tracks
    gEventCounter++;
    gEventNumber++;
  } // end loop over events
    
  runLoader->Delete();
}
