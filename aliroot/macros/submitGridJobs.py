#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import sys
import json
import subprocess
import time
import random
import re

try:
    from alienpy import alien
except Exception:
    try:
        from xjalienfs import alien
    except Exception:
        print("Can't load alienpy, exiting...")
        sys.exit(1)
        
alien.setup_logging()
wb = alien.InitConnection()
j = alien.AliEn()        

jdlExecutable = "/home/iarsene/work/202211_FOCALQA/RunFOCALanalysisWithEmbedding.sh"
gridHomeDir = "/alice/cern.ch/user/i/iarsene/"
gridWorkDir = "work/focalAnalysis/testEmbedding/"

def sendmessage(message):
    subprocess.Popen(['notify-send', message])    
    return

if len(sys.argv) == 1:
        print("Nothing to do, exit")
        sys.exit()

if len(sys.argv) == 2 and sys.argv[1]=="killall":
        returnObj = j.run("ps")
        inputJobList = str(returnObj.out)
        jobNumbers = []
        for line in inputJobList.split("\n"):
                jobNumber = 0
                isAlive = True
                for token in line.split(" "):
                        if len(re.findall('\d',token)) == 10:
                                jobNumber = int(token)
                        if token == "K":
                                isAlive = False
                if isAlive:
                        jobNumbers.append(jobNumber)
        if len(jobNumbers)==0:
                print("No jobs to kill")
                sys.exit()
        for job in jobNumbers:
                print("Kill job number ", job)
                j.run("kill "+str(job))
        sys.exit()
if len(sys.argv) == 2:
        print("Not a known command: ", sys.argv[1])
        sys.exit()

        
jdlFile = sys.argv[1]
gridOutputDir = sys.argv[2]
geometryFile = sys.argv[3]
parametersFile = sys.argv[4]
analysisMacros = sys.argv[5]
inputDataSetDir = sys.argv[6]
inputBkgDataSetDir = sys.argv[7]
bkgNeventsPerBkgFile = sys.argv[8]

returnObj = j.run("ls "+str(inputDataSetDir))
inputFiles = str(returnObj.out)
filesList = []
for inFile in inputFiles.split("\n"):
        filesList.append(str(inFile))
        
returnObj = j.run("ls "+str(inputBkgDataSetDir))
inputBkgFiles = str(returnObj.out)
filesListBkg = []
for inFile in inputBkgFiles.split("\n"):
        filesListBkg.append(str(inFile))

# create working directory and copy all needed files
j.run("mkdir -p "+str(gridHomeDir)+str(gridWorkDir))
commandToRun = f'cp file://{geometryFile} alien://{gridHomeDir}{gridWorkDir}geometry.txt'
subprocess.run(['alien.py',commandToRun])
commandToRun = f'cp file://{parametersFile} alien://{gridHomeDir}{gridWorkDir}parameters.txt'
subprocess.run(['alien.py',commandToRun])
commandToRun = f'cp file://{jdlExecutable} alien://{gridHomeDir}{gridWorkDir}'
subprocess.run(['alien.py',commandToRun])
commandToRun = f'cp file://{jdlFile} alien://{gridHomeDir}{gridWorkDir}'
subprocess.run(['alien.py',commandToRun])
# extract the macros from the argument and copy them to grid 
runMacro = ""
for macroName in analysisMacros.split(","):
        commandToRun = f'cp file://{macroName} alien://{gridHomeDir}{gridWorkDir}'
        subprocess.run(['alien.py',commandToRun])
        if runMacro == "":
                runMacro = macroName

# start job submission
counter = 0
for inFile in filesList:
        # create the xml collection for just this file
        print("Create xml collection for file",inFile)
        commandToRun = f'find -x {gridWorkDir}collection{inFile}.xml {inputDataSetDir} {inFile}/root_archive.zip'
        j.run(commandToRun)
        # choose randomly one of the files from the bkg data set
        bkgFileToUse = random.randrange(len(filesListBkg))
        bkgFileName = f'{inputBkgDataSetDir}/{filesListBkg[bkgFileToUse]}/root_archive.zip'
        print("bkg file is ", bkgFileName)
        # submit the grid job
        commandToRun = f'submit {gridHomeDir}{gridWorkDir}{jdlFile} {gridHomeDir}{gridWorkDir} collection{inFile}.xml {gridOutputDir}/{inFile} {runMacro} {bkgFileName} {bkgNeventsPerBkgFile}'
        j.run(commandToRun)
        counter += 1
        
