/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//
//
#include "TNamed.h"
#include "AliCDBEntry.h"
#include "AliFOCAL.h"
#include "AliFOCALCalibData.h"


ClassImp(AliFOCALCalibData)

AliFOCALCalibData::AliFOCALCalibData()
{
  // Default constructor
  Reset();
}
// ----------------------------------------------------------------- //
AliFOCALCalibData::AliFOCALCalibData(const char* name)
{
  //constructor
  TString namst = "Calib_";
  namst += name;
  SetName(namst.Data());
  SetTitle(namst.Data());
  Reset();
  
}
// ----------------------------------------------------------------- //
AliFOCALCalibData::AliFOCALCalibData(const AliFOCALCalibData &calib) :
  TNamed(calib)
{
  // copy constructor
  SetName(calib.GetName());
  SetTitle(calib.GetName());
  Reset();
  for(Int_t isec = 0; isec < kSec; isec++)
  {
    for(Int_t iseg = 0; iseg < kSeg; iseg++)
      {
	for(Int_t irow = 0; irow < kRow; irow++)
	  {
	    {
	      for(Int_t icol = 0; icol < kCol; icol++)
		{
		  fGainFactor[isec][iseg][irow][icol] =
		    calib.GetGainFactor(isec, iseg, irow, icol);
		}
	    }
	  }
      }
  }
}
// ----------------------------------------------------------------- //
AliFOCALCalibData &AliFOCALCalibData::operator =(const AliFOCALCalibData &calib)
{
  //asignment operator
  SetName(calib.GetName());
  SetTitle(calib.GetName());
  Reset();
  for(Int_t isec = 0; isec < kSec; isec++)
  {
    for(Int_t iseg = 0; iseg < kSeg; iseg++)
      {
	for(Int_t irow = 0; irow < kRow; irow++)
	  {
	    {
	      for(Int_t icol = 0; icol < kCol; icol++)
		{
		  fGainFactor[isec][iseg][irow][icol] =
		    calib.GetGainFactor(isec, iseg, irow, icol);
		}
	    }
	  }
      }
  }
  return *this;
}
// ----------------------------------------------------------------- //
AliFOCALCalibData::~AliFOCALCalibData()
{
  //destructor
}
// ----------------------------------------------------------------- //
void AliFOCALCalibData::Reset()
{
  for(Int_t isec = 0; isec < kSec; isec++)
  {
    for(Int_t iseg = 0; iseg < kSeg; iseg++)
      {
	for(Int_t irow = 0; irow < kRow; irow++)
	  {
	    {
	      for(Int_t icol = 0; icol < kCol; icol++)
		{
		  fGainFactor[isec][iseg][irow][icol] = 1;
		}
	    }
	  }
      }
  }
}
// ----------------------------------------------------------------- //
Float_t AliFOCALCalibData:: GetGainFactor(Int_t sec, Int_t seg, Int_t row, Int_t col) const
{
  return fGainFactor[sec][seg][row][col];
}
// ----------------------------------------------------------------- //

void AliFOCALCalibData::SetGainFactor(Int_t sec, Int_t seg, Int_t row, Int_t col, 
				      Float_t gain)
{

    fGainFactor[sec][seg][row][col] = gain;
}
// ----------------------------------------------------------------- //
