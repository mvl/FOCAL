/**************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include "AliFOCALhit.h"
#include <TClonesArray.h>
#include "Riostream.h"
#include "Rtypes.h"

ClassImp(AliFOCALhit)
  
//_____________________________________________________________________________
AliFOCALhit::AliFOCALhit():
  //  fEnergySum(0.),
    //  fNhit(50),
      //  fEnergy(0x0),
	//  fTime(0x0)
  fEnergy(0),
  fTime(0)
{
  for (Int_t i=0; i<4; i++)
    {
      fVolume[i] = 0;
    }
  //  fEnergy = new Float_t[fNhit];
  //  fTime = new Float_t[fNhit];
  //  for(Int_t i=0;i<fNhit; i++){
  //    fEnergy[i]= 0;
  //    fTime[i]= 0;
  //  }

}
//_____________________________________________________________________________
AliFOCALhit::AliFOCALhit(Int_t shunt,Int_t track, Int_t *vol, Float_t *hits):
  AliHit(shunt, track),
  fEnergy(hits[3])
{
  //
  // Add a FOCAL hit
  //
  Int_t i;
  for (i=0; i<4; i++) fVolume[i] = vol[i];
  fX=hits[0];
  fY=hits[1];
  fZ=hits[2];
  fEnergy = hits[3];
  fTime = hits[4];
}
//_____________________________________________________________________________
AliFOCALhit::AliFOCALhit(AliFOCALhit* oldhit):
  fEnergy(0.)
{
  *this=*oldhit;
}

//_____________________________________________________________________________
int AliFOCALhit::operator == (AliFOCALhit &cell) const
{
  Int_t i;
  if(fTrack!=cell.GetTrack()) return 0;
  for (i=0; i<4; i++) if(fVolume[i]!=cell.GetVolume(i)) return 0;
  return 1;
}

Int_t AliFOCALhit::Compare(const TObject * obj) const {
  
  if (this == obj) return 0;
  if (strcmp(this->ClassName(),obj->ClassName()) == 0) {
    AliFOCALhit * hit = (AliFOCALhit*) obj;
    if (fIndex == hit->fIndex) return 0;
    return fIndex > hit->fIndex ? 1 : -1;
  } 
  else
    return 1;
}
