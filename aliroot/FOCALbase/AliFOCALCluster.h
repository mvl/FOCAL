#ifndef ALIFOCALCLUSTER_H
#define ALIFOCALCLUSTER_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  16 2019                             //
//                                                     //
//  FOCAL Cluster class for FOCAL                      //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//

#include "Rtypes.h"

// --- ROOT system ---
#include <TVector3.h>
class TGeoManager;
class TGeoPhysicalNode;
class TPad;
class TPaveText;
class TGraph;
class Riostream;
// --- Standard library ---

// --- AliRoot header files ---

#include "AliCluster.h"
class AliFOCALGeometry;

class AliFOCALCluster : public AliCluster 
{
	public:
		AliFOCALCluster();
		AliFOCALCluster(Double_t x, Double_t y, Double_t z, Double_t e, Int_t segment = -1, Float_t seedE = 0, Int_t nCells = 0); // overload constructor
		AliFOCALCluster(const AliFOCALCluster &clst);
		AliFOCALCluster& operator= (const AliFOCALCluster &clst);
		virtual ~AliFOCALCluster();
	
		void Add(AliFOCALCluster const *clust);
		void MoveToZ(Float_t Z);
		void SetSegment(int id) { fSegment = id; }
		void SetE(Float_t val) { fEnergy = val; }
		Bool_t  Flag() const {return fVariable;} // user defined  
		void SetWeight(Float_t val) { fWeight = val; }
		void AddWeight(Float_t val) { fWeight += val; }
		void SetSegIndex(Int_t idx, Int_t val);
		void SetWidth1(Int_t idx, Float_t val);
		void SetWidth2(Int_t idx, Float_t val);
		void SetPhi(Int_t idx, Float_t val);
		void SetSegmentEnergy(Int_t idx, Float_t val);
		void SetSeedEnergy(Int_t idx, Float_t seedE) { fSeedEnergy[idx] = seedE; }
		void SetNcells(Int_t idx, Int_t nCell) { fNcells[idx] = nCell; }
		void SetHCALEnergy(Float_t e) { fHCALEnergy = e; }
		void SetIsoEnergyR2(Float_t e) { fEisoR2 = e; }
		void SetIsoEnergyR4(Float_t e) { fEisoR4 = e; }

		Int_t Segment() const { return fSegment; }
		Float_t X() const { return GetX(); }
		Float_t Y() const { return GetY(); }
		Float_t Z() const { return GetZ(); }
		Float_t E() const { return fEnergy; }
		void SetFlag(bool val){fVariable = val; }
		Float_t Weight() const { return fWeight; }
		Int_t GetSegIndex(Int_t idx) const;
		Float_t GetWidth1(Int_t idx) const;
		Float_t GetWidth2(Int_t idx) const;
		Float_t GetPhi(Int_t idx) const;
		Float_t GetSegmentEnergy(Int_t idx) const;
		Float_t GetSeedEnergy(Int_t idx) const { return fSeedEnergy[idx]; }
		Int_t GetNcells(Int_t idx) const { return fNcells[idx]; }
		Float_t GetHCALEnergy() const { return fHCALEnergy; }
		Float_t GetIsoEnergyR2() const { return fEisoR2; }
		Float_t GetIsoEnergyR4() const { return fEisoR4; }
		
		static const Int_t N_DEPTH_FIELDS = 10;
		
	private:
	
		AliFOCALGeometry* fGeom;  //! Pointer to geometry for utilities
		
		Int_t   fSegment;
		Float_t fEnergy;
		Bool_t  fVariable; 
		TVector3 fGlobPos;
		Float_t fWeight;
		Int_t fSegIndex [N_DEPTH_FIELDS];  // Segment numbers for per-Segment info
		Float_t fWidth1 [N_DEPTH_FIELDS];  // Major axis width (per segment)
		Float_t fWidth2 [N_DEPTH_FIELDS];  // Minor axis width (per segment)
		Float_t fPhi[N_DEPTH_FIELDS]; // In-plane orientation (per segment) 
		Float_t fSegmentEnergy [N_DEPTH_FIELDS]; // Energy per segment
		Float_t fHCALEnergy; // HCAL energy  (for final clusters)
		Float_t fEisoR2;  // Isolation energy with R=0.2 (for final clusters)
		Float_t fEisoR4;  // Isolation energy with R=0.4 (for final clusters)
                Float_t fSeedEnergy[N_DEPTH_FIELDS];  // Seed energy (per segment)
                Int_t   fNcells[N_DEPTH_FIELDS];      // number of cells (per segment)

	ClassDef(AliFOCALCluster,7) // Utility class for the detector set:FOCAL
};

#endif
