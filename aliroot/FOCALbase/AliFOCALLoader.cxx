/**************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

/* $Id: AliFOCALLoader.cxx 11795 2005-06-17 09:43:58Z alibrary $ */

#include "AliFOCALLoader.h"
#include "AliLog.h"

const TString AliFOCALLoader::fgkDefaultHitsFileName      = "FOCAL.Hits.root";
const TString AliFOCALLoader::fgkDefaultSDigitsFileName   = "FOCAL.SDigits.root";
const TString AliFOCALLoader::fgkDefaultDigitsFileName    = "FOCAL.Digits.root";
const TString AliFOCALLoader::fgkDefaultRecPointsFileName = "FOCAL.RecPoints.root";
const TString AliFOCALLoader::fgkDefaultTracksFileName    = "FOCAL.Tracks.root";


ClassImp(AliFOCALLoader)
AliFOCALLoader::AliFOCALLoader()
 {
 }
/*****************************************************************************/ 
AliFOCALLoader::AliFOCALLoader(const Char_t *name,const Char_t *topfoldername)
 :AliLoader(name,topfoldername)
{
  AliDebug(1,Form("Name = %s; topfolder = %s",name,topfoldername));

}
/*****************************************************************************/ 

AliFOCALLoader::AliFOCALLoader(const Char_t *name,TFolder *topfolder)
  :AliLoader(name,topfolder)
 {
 }

