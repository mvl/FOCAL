#ifndef ALIFOCALPAD_H
#define ALIFOCALPAD_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  30 2009                             //
//                                                     //
//  Utility class for FOCAL                            //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//

#include "Rtypes.h"
#include "TObject.h"
class TClonesArray;



class AliFOCALPad: public TObject
{
	public:
		AliFOCALPad();
		AliFOCALPad(Int_t id, Int_t row, Int_t col, Int_t layer, Int_t segment, Int_t tower, Float_t x, Float_t y, Float_t z, Float_t e);
		AliFOCALPad(Int_t Pad, Int_t Row, Int_t Col, Int_t Layer, Int_t Segment, Int_t Tower, Float_t X, Float_t Y, Float_t Z, Float_t E, bool IsPixel);
		AliFOCALPad(AliFOCALPad *fPad);
		AliFOCALPad(const AliFOCALPad &fPad);
		AliFOCALPad &operator=(const AliFOCALPad &fPad);
		virtual ~AliFOCALPad();
		
		virtual void PadID(int id){fPadid = id;}
		virtual void SetRow(int id){fRow = id;}
		virtual void SetCol(int id){fCol = id;}
		virtual void SetLayer(int id){fLayer = id;}
		virtual void SetSegment(int id){fSegment = id;}
		virtual void SetTower(int id){fTower = id;}
		virtual void SetX(Float_t val){fX = val;}
		virtual void SetY(Float_t val){fY = val;}
		virtual void SetZ(Float_t val){fZ = val;}
		virtual void SetE(Float_t val){fE = val;}
		virtual void SetIsPixel(bool val){fIsPixel = val;}
		
		virtual Int_t PadID() const {return fPadid;}
		virtual Int_t Row() const {return fRow;}
		virtual Int_t Col() const {return fCol;}
		virtual Int_t Layer() const {return fLayer;}
		virtual Int_t Segment() const {return fSegment;}
		virtual Int_t Tower() const {return fTower;}
		virtual Float_t X() const {return fX; }
		virtual Float_t Y() const {return fY; }
		virtual Float_t Z() const {return fZ; }
		virtual Float_t E() const {return fE; }
		virtual bool  IsPixel() const {return fIsPixel; }
		
	private:
		Int_t fPadid;  // pad id in the wafer (NxN pads)  
		Int_t fRow ;    // x location of the wafer in the brick
		Int_t fCol ;    // y location of the wafer in the brick 
		Int_t fLayer;   // location of layers in the segments
		Int_t fSegment; // segments (0-2)
		Int_t fTower;   // brick number 
		Float_t fX;
		Float_t fY;
		Float_t fZ;
		Float_t fE;
		bool fIsPixel; // True if this is a pixels, False if this is a pad
		
	ClassDef(AliFOCALPad,4) // Utility class for the detector set:FOCAL
};

#endif
