#include "AliFOCALCellMap.h"

AliFOCALCellMap::AliFOCALCellMap(): 
  fRows(0),
  fCols(0),
  fSegment(0)
{
  fCellMap = 0;
}

AliFOCALCellMap::AliFOCALCellMap(Int_t segment, Int_t cols, Int_t rows): 
  fRows(rows),
  fCols(cols),
  fSegment(segment)
{
  fCellMap = new AliFOCALCell * [fCols*fRows];
  for (Long_t i = 0; i < fCols*fRows ; i++) {
    fCellMap[i] = 0;
  }
}

AliFOCALCellMap& AliFOCALCellMap::operator=(const AliFOCALCellMap &fMap)
{
  if(this!=&fMap){
    fRows = fMap.fRows;
    fCols = fMap.fCols;
    fSegment = fMap.fSegment;
    fCellMap = new AliFOCALCell * [fCols*fRows];
    for (Long_t i = 0; i < fCols*fRows ; i++) {
      fCellMap[i] = fMap.fCellMap[i];
    }
  } 
  return *this;
}

AliFOCALCellMap::~AliFOCALCellMap() {
  delete [] fCellMap;
}

void AliFOCALCellMap::CreateMap(Int_t segment, Int_t nCol, Int_t nRow) {
  
  fSegment = segment;
  fCols = nCol;
  fRows = nRow;
  
  fCellMap = new AliFOCALCell * [nCol*nRow];
  for (Long_t i = 0; i < fCols*fRows ; i++) {
    fCellMap[i] = 0;
  }
}

void AliFOCALCellMap::ResetMap() {

  for (Long_t i = 0; i < fCols*fRows ; i++) {
    fCellMap[i] = 0;
  }
}
