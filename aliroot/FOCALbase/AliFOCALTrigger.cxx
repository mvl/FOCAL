/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

#include <iostream>

#include "Riostream.h"
#include "Rtypes.h"
#include "AliFOCALTrigger.h"
#include "TClonesArray.h"
#include "TIterator.h"
#include "AliFOCALdigit.h"

ClassImp(AliFOCALTrigger)

using namespace std;

//__________________________________________________________________________
AliFOCALTrigger::AliFOCALTrigger(): 
  TObject(),
  fGeom(0x0),
  fComputedTriggers(),
  fPadRegionThreshold(0.0),
  fFiredPixelSensors(),
  fPadTriggerZones(),
  fPadToPixel(),
  fLocalTrigger(kNoTrigger)
  //fPixelSensorRanges()
{}

//__________________________________________________________________________
AliFOCALTrigger::AliFOCALTrigger(const AliFOCALTrigger& other):
  fGeom(other.fGeom),
  fPadRegionThreshold(other.fPadRegionThreshold),
  fFiredPixelSensors(other.fFiredPixelSensors),
  fPadTriggerZones(other.fPadTriggerZones),
  fPadToPixel(other.fPadToPixel),
  fLocalTrigger(other.fLocalTrigger)
  //fPixelSensorRanges(other.fPixelSensorRanges)
{
  for (int i = kLocalFull; i < kNTriggers; i++) {
    fComputedTriggers[i] = other.fComputedTriggers[i];
  }
}

//__________________________________________________________________________
AliFOCALTrigger& AliFOCALTrigger::operator=(const AliFOCALTrigger& other)
{
  //Assignment operator 
  if(this != &other) {
    fGeom = other.fGeom;
    fPadRegionThreshold = other.fPadRegionThreshold;
    fFiredPixelSensors = other.fFiredPixelSensors;
    fPadTriggerZones = other.fPadTriggerZones;
    fPadToPixel = other.fPadToPixel;
    fLocalTrigger = other.fLocalTrigger;
    //fPixelSensorRanges = other.fPixelSensorRanges;

    for (int i = kLocalFull; i < kNTriggers; i++) {
      fComputedTriggers[i] = other.fComputedTriggers[i];
    }
  }
  return *this;
}

//__________________________________________________________________________
AliFOCALTrigger::~AliFOCALTrigger()
{
  // Default Destructor
}

//__________________________________________________________________________
int AliFOCALTrigger::GetPixelSensor(int iCol, int iRow, int padWaferCol, int padWaferRow, int nRowPix) {
  return (padWaferCol * 3 + iCol) * nRowPix +  (padWaferRow * 6 + iRow);
}

//__________________________________________________________________________
void AliFOCALTrigger::BuildLUTs()
{
  fPadTriggerZones.clear();
  fPadToPixel.clear();
  //fPixelSensorRanges.clear();

  int nColPix = fGeom->GetNumberOfPIXsInX() * fGeom->GetNumberOfTowersInX();      //  15 x 2; number of pixel sensor columns
  int nRowPix = 2 * fGeom->GetNumberOfPIXsInY() * fGeom->GetNumberOfTowersInY();  //  2 x 3 x 11;  number of pixel sensor rows  
  //int ncolsPerPix = fGeom->GetGlobalPixelWaferSizeX() / fGeom->GetVirtualPadSize(1);  // 3 cm / 500 um;  number of macro-pixel columns per sensor
  //int nrowsPerPix = 0.5 * fGeom->GetGlobalPixelWaferSizeY() / fGeom->GetVirtualPadSize(1);      // 2.74 / 2 cm / 500 um; number of macro-pixel rows per sensor
  cout << "+++++++++++++++ AliFOCALTrigger::BuildLUTs() nColPix, nRowPix: " << nColPix << ", " << nRowPix << endl;

  // nrows and ncols for pad layers
  //int nrows, ncols;     // 88, 90       // number of rows and columns in pad layers
  //fGeom->GetVirtualNColRow(0, ncols, nrows);
  int ncols = fGeom->GetNumberOfPADsInX() * fGeom->GetNumberOfTowersInX() * fGeom->GetNumberOfPADXinWafer();
  int nrows = fGeom->GetNumberOfPADsInY() * fGeom->GetNumberOfTowersInY() * fGeom->GetNumberOfPADYinWafer();
  cout << "+++++++++++++++ AliFOCALTrigger::BuildLUTs() ncols, nrows: " << ncols << ", " << nrows << endl;

  int nPadTrigRows = nrows / 2;             // 44 trigger regions in row direction (4 x 11)
  
  // Make a map element for each pad sensor with the value being the pad trigger zone to which this pad belongs
  for (int icol = 0; icol < ncols; icol++) {
    for (int irow = 0; irow < nrows; irow++) {
      int irowTrig = irow / 2;    // 2 rows per trigger zone
      int icolTrig = icol / 9;    // this gives the pad wafer in the column direction
      int irowLocal = irow % 8;        // local pad
      int icolLocal = icol % 9;        // local pad
      int icolTrigLocal = 0;
      if (irowLocal == 0 || irowLocal == 3 || irowLocal == 4 || irowLocal == 7) {
        if (icolLocal >= 0 && icolLocal < 5) {
          icolTrigLocal = 0;
        } else {
          icolTrigLocal = 1;
        }
      } else {
        if (icolLocal >= 0 && icolLocal < 4) {
          icolTrigLocal = 0;
        } else {
          icolTrigLocal = 1;
        }
      }
      icolTrig = icolTrig * 2 + icolTrigLocal;

      std::pair<int,int> pad = std::make_pair(icol, irow);
      fPadTriggerZones[pad] = icolTrig * nPadTrigRows + irowTrig;
    }
  }

  if (fLocalTrigger == kLocalFull) {
    BuildLUTLocalFull();
  }
  if (fLocalTrigger == kLocal1) {
    BuildLUTLocal1();
  }

}

//_______________________________________________________________________________________________________________________
void AliFOCALTrigger::BuildLUTLocal1() {

  //
  // Creates a pad trigger region -> pixel sensors  map pixel sensors from regions overlappng with the pad trigger region
  //    Trigger regions per module
  //    Inner barrel: 12 bits (2 x 6) -> corresponds to pixel strings of 3 sensors / 1 pad wafer
  //    Outer barrel: 6 bits (1 x 6) -> corresponds to pixel strings of 9 sensors / 3 pad wafers

  int nRowPix = 2 * fGeom->GetNumberOfPIXsInY() * fGeom->GetNumberOfTowersInY();  //  2 x 3 x 11;  number of pixel sensor rows  
  int ncols = fGeom->GetNumberOfPADsInX() * fGeom->GetNumberOfTowersInX() * fGeom->GetNumberOfPADXinWafer();
  int nrows = fGeom->GetNumberOfPADsInY() * fGeom->GetNumberOfTowersInY() * fGeom->GetNumberOfPADYinWafer();
  cout << "+++++++++++++++ AliFOCALTrigger::BuildLUTs() ncols, nrows: " << ncols << ", " << nrows << endl;

  int nPadTrigRows = nrows / 2;             // 44 trigger regions in row direction (4 x 11)
  int nPadTrigCols = ncols / 9 * 2;         // 20 trigger regions in column direction (10 wafers x 2)

  // make a map for each pad trigger zone with the value being a vector of all pixel sensors overlapping with the pad trigger zone
  for (int icol = 0; icol < nPadTrigCols; icol++) {
    for (int irow = 0; irow < nPadTrigRows; irow++) {
      int padTrigger = icol * nPadTrigRows + irow;
      int localRow = irow % 4;

      int padWaferCol = icol / 2;
      int padWaferRow = irow / 4;

      bool isLeftOB = (padWaferCol < 3);
      bool isRightOB = (padWaferCol > 6);
      bool isOB = (isLeftOB || isRightOB);

      int pixRowLow = 0;
      int pixRowHigh = 1;
      if (localRow == 1) {
        pixRowLow = 1;
        pixRowHigh = 2;
      }
      if (localRow == 2) {
        pixRowLow = 3;
        pixRowHigh = 4;
      }
      if (localRow == 3) {
        pixRowLow = 4;
        pixRowHigh = 5;
      }

      std::vector<int> pixSensors;
      for (int iPixCol = 0; iPixCol < 3; iPixCol++) {
        for (int iPixRow = pixRowLow; iPixRow <= pixRowHigh; iPixRow++) {
          if (isLeftOB) {
            for (int iPadWaferCol = 0; iPadWaferCol < 3; iPadWaferCol++) {
              pixSensors.push_back(GetPixelSensor(iPixCol, iPixRow, iPadWaferCol, padWaferRow, nRowPix));
            }
          }
          if (isRightOB) {
            for (int iPadWaferCol = 7; iPadWaferCol < 10; iPadWaferCol++) {
              pixSensors.push_back(GetPixelSensor(iPixCol, iPixRow, iPadWaferCol, padWaferRow, nRowPix));
            }
          }
          if (!isOB) {
            pixSensors.push_back(GetPixelSensor(iPixCol, iPixRow, padWaferCol, padWaferRow, nRowPix));
          }
        }
      }
      fPadToPixel[padTrigger] = pixSensors;
    }
  }
}

//_______________________________________________________________________________________________________________________
void AliFOCALTrigger::BuildLUTLocalFull() {

  //
  // Creates a pad trigger region -> pixel sensors  map which fires just the pixel sensors overlapping with the pad trigger region
  //    This it seems is unfeasable in reality, but can be used for checks
  int nRowPix = 2 * fGeom->GetNumberOfPIXsInY() * fGeom->GetNumberOfTowersInY();  //  2 x 3 x 11;  number of pixel sensor rows
  int ncols = fGeom->GetNumberOfPADsInX() * fGeom->GetNumberOfTowersInX() * fGeom->GetNumberOfPADXinWafer();
  int nrows = fGeom->GetNumberOfPADsInY() * fGeom->GetNumberOfTowersInY() * fGeom->GetNumberOfPADYinWafer();
  cout << "+++++++++++++++ AliFOCALTrigger::BuildLUTs() ncols, nrows: " << ncols << ", " << nrows << endl;

  int nPadTrigRows = nrows / 2;             // 44 trigger regions in row direction (4 x 11)
  int nPadTrigCols = ncols / 9 * 2;         // 20 trigger regions in column direction (10 wafers x 2)

  // make a map for each pad trigger zone with the value being a vector of all pixel sensors overlapping with the pad trigger zone
  for (int icol = 0; icol < nPadTrigCols; icol++) {
    for (int irow = 0; irow < nPadTrigRows; irow++) {
      int padTrigger = icol * nPadTrigRows + irow;
      int localCol = icol % 2;
      int localRow = irow % 4;

      int padWaferCol = icol / 2;
      int padWaferRow = irow / 4;
      
      if (localCol == 0) {
        if (localRow == 0) {
          std::vector<int> pixSensors;
          // (0,0)
          pixSensors.push_back(GetPixelSensor(0, 0, padWaferCol, padWaferRow, nRowPix));
          // (0,1)
          pixSensors.push_back(GetPixelSensor(0, 1, padWaferCol, padWaferRow, nRowPix));
          // (1,0)
          pixSensors.push_back(GetPixelSensor(1, 0, padWaferCol, padWaferRow, nRowPix));
          // (1,1)
          pixSensors.push_back(GetPixelSensor(1, 1, padWaferCol, padWaferRow, nRowPix));
          fPadToPixel[padTrigger] = pixSensors;
        }
        if (localRow == 1) {
          std::vector<int> pixSensors;
          // (0,1)
          pixSensors.push_back(GetPixelSensor(0, 1, padWaferCol, padWaferRow, nRowPix));
          // (0,2)
          pixSensors.push_back(GetPixelSensor(0, 2, padWaferCol, padWaferRow, nRowPix));
          // (1,1)
          pixSensors.push_back(GetPixelSensor(1, 1, padWaferCol, padWaferRow, nRowPix));
          // (1,2)
          pixSensors.push_back(GetPixelSensor(1, 2, padWaferCol, padWaferRow, nRowPix));
          fPadToPixel[padTrigger] = pixSensors;
        }
        if (localRow == 2) {
          std::vector<int> pixSensors;
          // (0,3)
          pixSensors.push_back(GetPixelSensor(0, 3, padWaferCol, padWaferRow, nRowPix));
          // (0,4)
          pixSensors.push_back(GetPixelSensor(0, 4, padWaferCol, padWaferRow, nRowPix));
          // (1,3)
          pixSensors.push_back(GetPixelSensor(1, 3, padWaferCol, padWaferRow, nRowPix));
          // (1,4)
          pixSensors.push_back(GetPixelSensor(1, 4, padWaferCol, padWaferRow, nRowPix));
          fPadToPixel[padTrigger] = pixSensors;
        }
        if (localRow == 3) {
          std::vector<int> pixSensors;
          // (0,4)
          pixSensors.push_back(GetPixelSensor(0, 4, padWaferCol, padWaferRow, nRowPix));
          // (0,5)
          pixSensors.push_back(GetPixelSensor(0, 5, padWaferCol, padWaferRow, nRowPix));
          // (1,4)
          pixSensors.push_back(GetPixelSensor(1, 4, padWaferCol, padWaferRow, nRowPix));
          // (1,5)
          pixSensors.push_back(GetPixelSensor(1, 5, padWaferCol, padWaferRow, nRowPix));
          fPadToPixel[padTrigger] = pixSensors;
        }
      }
      if (localCol == 1) {
        if (localRow == 0) {
          std::vector<int> pixSensors;
          // (1,0)
          pixSensors.push_back(GetPixelSensor(1, 0, padWaferCol, padWaferRow, nRowPix));
          // (1,1)
          pixSensors.push_back(GetPixelSensor(1, 1, padWaferCol, padWaferRow, nRowPix));
          // (2,0)
          pixSensors.push_back(GetPixelSensor(2, 0, padWaferCol, padWaferRow, nRowPix));
          // (2,1)
          pixSensors.push_back(GetPixelSensor(2, 1, padWaferCol, padWaferRow, nRowPix));
          fPadToPixel[padTrigger] = pixSensors;
        }
        if (localRow == 1) {
          std::vector<int> pixSensors;
          // (1,1)
          pixSensors.push_back(GetPixelSensor(1, 1, padWaferCol, padWaferRow, nRowPix));
          // (1,2)
          pixSensors.push_back(GetPixelSensor(1, 2, padWaferCol, padWaferRow, nRowPix));
          // (2,1)
          pixSensors.push_back(GetPixelSensor(2, 1, padWaferCol, padWaferRow, nRowPix));
          // (2,2)
          pixSensors.push_back(GetPixelSensor(2, 2, padWaferCol, padWaferRow, nRowPix));
          fPadToPixel[padTrigger] = pixSensors;
        }
        if (localRow == 2) {
          std::vector<int> pixSensors;
          // (1,3)
          pixSensors.push_back(GetPixelSensor(1, 3, padWaferCol, padWaferRow, nRowPix));
          // (1,4)
          pixSensors.push_back(GetPixelSensor(1, 4, padWaferCol, padWaferRow, nRowPix));
          // (2,3)
          pixSensors.push_back(GetPixelSensor(2, 3, padWaferCol, padWaferRow, nRowPix));
          // (2,4)
          pixSensors.push_back(GetPixelSensor(2, 4, padWaferCol, padWaferRow, nRowPix));
          fPadToPixel[padTrigger] = pixSensors;
        }
        if (localRow == 3) {
          std::vector<int> pixSensors;
          // (1,4)
          pixSensors.push_back(GetPixelSensor(1, 4, padWaferCol, padWaferRow, nRowPix));
          // (1,5)
          pixSensors.push_back(GetPixelSensor(1, 5, padWaferCol, padWaferRow, nRowPix));
          // (2,4)
          pixSensors.push_back(GetPixelSensor(2, 4, padWaferCol, padWaferRow, nRowPix));
          // (2,5)
          pixSensors.push_back(GetPixelSensor(2, 5, padWaferCol, padWaferRow, nRowPix));
          fPadToPixel[padTrigger] = pixSensors;
        }
      }
    }
  }

}

//__________________________________________________________________________
void AliFOCALTrigger::ComputeTriggers(TClonesArray* digits) {
  //
  // Compute here all initialized triggers 
  for (int i = 0; i < kNTriggers; i++) {
    fComputedTriggers[i] = false;
  }

  AliFOCALdigit* digit = nullptr;
  TIter iterDigits(digits);

  std::map<int,double> trigEnDep;
  std::vector<int> triggerLayers = {5};

  cout << " +++++++++++++++++++++++++++++++++++++++++ AliFOCALTrigger::ComputeTriggers(), loop over " << digits->GetEntries() << " digits" << endl;
  for(int i=0; i<digits->GetEntries(); i++) {
    digit = (AliFOCALdigit*)iterDigits();
    //cout << "============ digit " << i << " (col,row,layer,amp): " << digit->GetCol() << ", " << digit->GetRow() << ", " << digit->GetLayer() << ", " << digit->GetAmp() << endl;
    bool correctLayer = false;
    for (auto& ilayer : triggerLayers) {
      if (digit->GetLayer() == ilayer) {
        correctLayer = true;
        break;
      }
    }
    if (!correctLayer) {
      continue;
    }
    //cout << "============ good layer" << endl;

    int col = digit->GetCol();
    int row = digit->GetRow();
    float x, y, z;
    fGeom->GetXYZFromColRowSeg(col, row, 0, x, y, z);
    //cout << "============ x, y " << x << ", " << y << endl;
    std::pair<int,int> sensor = fGeom->GetPadSensor(x, y);
    int sensorCol = sensor.first;
    int sensorRow = sensor.second;
    //cout << "============ sensorCol, sensorRow: " << sensorCol << ", " << sensorRow << endl;
    
    std::pair<int,int> pad = std::make_pair(sensorCol, sensorRow);
    int triggerRegion = fPadTriggerZones[pad];
    //cout << "============= trigger region " << triggerRegion << endl;
    if (trigEnDep.find(triggerRegion) != trigEnDep.end()) {
      trigEnDep[triggerRegion] += digit->GetAmp();
      //cout << "============= sum to existing, total " << trigEnDep[triggerRegion] << endl;
    } else {
      trigEnDep[triggerRegion] = digit->GetAmp();
      //cout << "============= new trigger region, size is now " << trigEnDep.size() << endl;
    }
  }

  int nPadTrigRows = 44;
  
  fFiredPixelSensors.clear();

  cout << " +++++++++++++++++++++++++++++++++++++++++ AliFOCALTrigger::ComputeTriggers(), loop over " << trigEnDep.size() << " pad trigger regions" << endl;
  for (auto& [padRegion, amp] : trigEnDep) {
    //cout << "----------- padRegion, amp: " << padRegion << ", " << amp << endl;
    if (amp > fPadRegionThreshold) {
      fComputedTriggers[kGlobal] = true;
      //icolTrig * nPadTrigRows + irowTrig
      int padTrigCol = padRegion / nPadTrigRows;
      int padTrigRow = padRegion % nPadTrigRows;
      //cout << "----------- over threshold, trig col/row " << padTrigCol << ", " << padTrigRow << endl;
      if (padTrigCol >= 4 && padTrigCol <= 15) {
        if (padTrigRow >= 8 && padTrigRow <= 35) {
          //cout << "----------- is inner " << endl;
          fComputedTriggers[kGlobalInner] = true;        
        } 
      }
      // if a local trigger is specified, mark it as fulfilled and tag the corresponding pixel sensors
      if (fLocalTrigger != kNoTrigger) {
        fComputedTriggers[fLocalTrigger] = true;
      }
      for (auto& pix : fPadToPixel[padRegion]) {
        //cout << "------------- tag pixel sensor" << pix << endl;
        fFiredPixelSensors[pix] = true;
      }
    }
  }
}

//______________________________________________________________________________________________________
void AliFOCALTrigger::PrintPixSensors(int triggerZone)
{
  auto sensors = fPadToPixel[triggerZone];
  cout << "List of pix sensors for trigger zone " << triggerZone << ": ";
  for (int sensor : sensors) {
    cout << sensor << " ";
  }
  cout << endl;
};
