/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July 16   2010                            //
//                                                     //
//  Clusterizerv0 for ALICE-FOCAL                      //
//                                                     //
//-----------------------------------------------------//

#include "Riostream.h"
#include "TMath.h"
#include "TText.h"
#include "TLine.h"

#include <stdio.h>
#include <math.h>

// --- ROOT system ---
#include <cassert>

class TROOT;
#include <TH1.h>
#include <TFile.h>
class TFolder;
#include <TMath.h>
#include <TMinuit.h>
#include <TTree.h>
class TSystem;
#include <TBenchmark.h>
#include <TBrowser.h>
#include <TROOT.h>

/// -- Aliroot 
#include "AliFOCALhit.h"
#include "AliFOCALCluster.h"
#include "AliFOCALGeometry.h"
#include "AliFOCALClusterizerv0.h"
#include "AliFOCALPad.h"
#include "AliFOCALCell.h"

#include "AliLog.h"
#include "AliRun.h"
#include "AliHit.h"
#include "AliDetector.h"
#include "AliRunLoader.h"
#include "AliLoader.h"
#include "AliConfig.h"
#include "AliMagF.h"
#include "AliDigitizer.h"
#include "AliHeader.h"
#include "AliCDBManager.h"
#include "AliCDBStorage.h"
#include "AliCDBEntry.h"

ClassImp(AliFOCALClusterizerv0)

//________________________________________________________________________//
AliFOCALClusterizerv0::AliFOCALClusterizerv0():
  AliFOCALClusterizer(),
  fGeom(0),
  fCHI2Limit(0),
  fNumberOfClusters(0),
  fNumberOfClustersItr(0),
  fClusteringEnergyThreshold(0),
  fLocalEnergyThreshold(0),
  fStripLocalEnergyThreshold(0),
  fStripClusteringEnergyThreshold(0)
{
  //Init();
}

//-----------------------------------------------------------------------//
AliFOCALClusterizerv0::AliFOCALClusterizerv0(const AliFOCALClusterizerv0 &ali):
  AliFOCALClusterizer(ali),
  fGeom(ali.fGeom),
  fCHI2Limit(ali.fCHI2Limit),
  fNumberOfClusters(ali.fNumberOfClusters),
  fNumberOfClustersItr(ali.fNumberOfClustersItr),
  fClusteringEnergyThreshold(ali.fClusteringEnergyThreshold),
  fLocalEnergyThreshold(ali.fLocalEnergyThreshold),
  fStripLocalEnergyThreshold(ali.fStripLocalEnergyThreshold),
  fStripClusteringEnergyThreshold(ali.fStripClusteringEnergyThreshold)
{
  //Init();
} 

//-----------------------------------------------------------------------//
AliFOCALClusterizerv0::AliFOCALClusterizerv0(AliFOCALClusterizerv0 *ali):
  AliFOCALClusterizer(),
  fGeom(0),
  fCHI2Limit(0),
  fNumberOfClusters(0),
  fNumberOfClustersItr(0),
  fClusteringEnergyThreshold(0),
  fLocalEnergyThreshold(0),
  fStripLocalEnergyThreshold(0),
  fStripClusteringEnergyThreshold(0)
{

	*this = ali;
}

//-----------------------------------------------------------------------//
AliFOCALClusterizerv0& AliFOCALClusterizerv0::operator=(const AliFOCALClusterizerv0 &ali)
{
	if(this!=&ali)
	{
		fGeom = ali.fGeom;
		fCHI2Limit = ali.fCHI2Limit;
		fNumberOfClusters = ali.fNumberOfClusters;
		fNumberOfClustersItr = ali.fNumberOfClustersItr;
		fClusteringEnergyThreshold = ali.fClusteringEnergyThreshold;
		fLocalEnergyThreshold = ali.fLocalEnergyThreshold;
		fStripLocalEnergyThreshold = ali.fStripLocalEnergyThreshold;
		fStripClusteringEnergyThreshold = ali.fStripClusteringEnergyThreshold;
		//Init();
	}
	return *this;
	
}

//-----------------------------------------------------------------------//
AliFOCALClusterizerv0::~AliFOCALClusterizerv0()
{
	// Default destructor
	delete fGeom;

}

//-----------------------------------------------------------------------//
void AliFOCALClusterizerv0::InitGeometry(char *geometryfile)
{
	fGeom = AliFOCALGeometry::GetInstance(geometryfile);
}
 
//-----------------------------------------------------------------------//
void AliFOCALClusterizerv0::InitGeometry()
{
	fGeom = AliFOCALGeometry::GetInstance();
}




