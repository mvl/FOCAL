#ifndef ALIFOCAL_H
#define ALIFOCAL_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

/* $Id: AliFOCAL.h 32357 2009-05-13 17:10:26Z hristov $ */

////////////////////////////////////////////////
//  Manager and hits classes for set:FOCAL      //
////////////////////////////////////////////////
 
#include "AliDetector.h"
#include "AliFOCALGeometry.h"

class AliLoader;
class TClonesArray;
class TFile;
class AliFOCALLoader;
class AliFOCALhit;
class AliFOCALDigitizer;
class AliFOCALGeometry;
class AliFOCALComposition;
class AliDigitizationInput;

class AliFOCAL : public AliDetector {

public:
  AliFOCAL();
  AliFOCAL(const char *name, const char *title);

  virtual AliLoader* MakeLoader(const char* topfoldername);

  virtual      ~AliFOCAL();
  virtual void  AddHit(Int_t track, Int_t* vol, Float_t* hits);
  virtual void  CreateGeometry() {}
  virtual void  CreateMaterials() {}
  virtual void  Init() {}
  virtual Int_t IsVersion() const =0;
  virtual void  StepManager();
  virtual void  MakeBranch(Option_t* option);
  virtual void  SetTreeAddress();

  virtual AliFOCALGeometry *GetGeometry(const char *geomfile) const
  {return AliFOCALGeometry::GetInstance(geomfile) ; }
  virtual AliFOCALGeometry *GetGeometry() const
  {return AliFOCALGeometry::GetInstance() ; }
  

  virtual void  Hits2SDigits();
  virtual void  SDigits2Digits();
  virtual void  Hits2Digits();

  virtual AliDigitizer* CreateDigitizer(AliDigitizationInput* manager) const;

  virtual void  Digits2Raw();
  virtual Bool_t Raw2SDigits(AliRawReader *rawReader);
  virtual Bool_t IsInFOCAL(Double_t x, Double_t y, Double_t z);

 protected:
  AliFOCALGeometry* fGeom;

 private:
  ClassDef(AliFOCAL,11);
};
#endif
