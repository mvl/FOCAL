/***************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July 30   2009                            //
//                                                     //
//  Cell code for ALICE-FOCAL                          //
//                                                     //
//-----------------------------------------------------//


#include "Riostream.h"
#include "TMath.h"
#include "TText.h"
#include "TLine.h"

#include <stdio.h>
#include <math.h>

#include "AliFOCALKinematics.h"

ClassImp(AliFOCALKinematics)

AliFOCALKinematics::AliFOCALKinematics():
  fEvent(0),
  fPid(0),
  fPx(0), 
  fPy(0),
  fPz(0),
  fRap(0),
  fEta(0),
  fMother(0),
  fAcc(0)
{
  //Default constructor
}

AliFOCALKinematics::AliFOCALKinematics(Int_t event, Int_t pid, float px, float py, float pz,
				       float rap, float eta, int mother, int acc):
  fEvent(event),
  fPid(pid),
  fPx(px), 
  fPy(py),
  fPz(pz),
  fRap(rap),
  fEta(eta),
  fMother(mother),
  fAcc(acc)
{
  //Default constructor
}

AliFOCALKinematics::AliFOCALKinematics(AliFOCALKinematics *fKinematics):
  fEvent(0),
  fPid(0),
  fPx(0), 
  fPy(0),
  fPz(0),
  fRap(0),
  fEta(0),
  fMother(0),
  fAcc(0)
{
  //Default constructor
  *this = fKinematics; 
}



AliFOCALKinematics::AliFOCALKinematics(const AliFOCALKinematics& fKinematics):
  TObject(fKinematics),
  fEvent(fKinematics.fEvent),
  fPid(fKinematics.fPid),
  fPx(fKinematics.fPx),
  fPy(fKinematics.fPy),
  fPz(fKinematics.fPz),
  fRap(fKinematics.fRap),
  fEta(fKinematics.fEta),
  fMother(fKinematics.fMother),
  fAcc(fKinematics.fAcc)
{
  //Default constructor
 }

AliFOCALKinematics & AliFOCALKinematics::operator=(const AliFOCALKinematics & fKinematics)
{
  //Assignment operator
  if(this != &fKinematics)
    {
      fEvent = fKinematics.fEvent;
      fPid = fKinematics.fPid ; 
      fPx = fKinematics.fPx ; 
      fPy = fKinematics.fPy ; 
      fPz = fKinematics.fPz ; 
      fRap = fKinematics.fRap ; 
      fEta = fKinematics.fEta ; 
      fMother = fKinematics.fMother ; 
      fAcc = fKinematics.fAcc ; 
    }
  return *this;
}



AliFOCALKinematics::~AliFOCALKinematics()
{
  // Default destructor
}



