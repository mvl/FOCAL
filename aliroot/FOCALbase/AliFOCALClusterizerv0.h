#ifndef ALIFOCALClusterizerv0_H
#define ALIFOCALClusterizerv0_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//                                                     //
//  Date   : July  16 2010                             //
//                                                     //
//  Utility class for FOCAL                            //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//
// Base class for the clusterization algorithm 

#include "AliFOCALClusterizer.h"

class AliFOCALCluster;
class AliFOCALGeometry;
class AliFOCALPad;
class AliFOCALCell;
class AliFOCALDigitizer ;
class AliFOCALhit;


class AliFOCALClusterizerv0 : public AliFOCALClusterizer 
{

 public:

  AliFOCALClusterizerv0() ;        // default constructor 
  AliFOCALClusterizerv0(const AliFOCALClusterizerv0 &ali);
  AliFOCALClusterizerv0(AliFOCALClusterizerv0 *ali);
  AliFOCALClusterizerv0 &operator=(const AliFOCALClusterizerv0 &ali);

  virtual ~AliFOCALClusterizerv0() ; // destructor 
  
  virtual const char * Version() const { return "clustering of FoCAL v0" ; }
  void InitGeometry(char *geometryfile);
  void InitGeometry();

 private:

  AliFOCALGeometry *fGeom;
  
  ClassDef(AliFOCALClusterizerv0,1);  // Clusterization algorithm class
} ;

#endif // AliFOCALLCLUSTERIZER_H

  




  
