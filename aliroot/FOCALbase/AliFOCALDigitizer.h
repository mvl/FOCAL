#ifndef ALIFOCALDIGITIZER_H
#define ALIFOCALDIGITIZER_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//  Header File : PMDDigitization.h, Version 00        //
//                                                     //
//  Date   : August 03 2009                         //
//                                                     //
//-----------------------------------------------------//
// Author - T. Gunji
//

#include "AliDigitizer.h"
#include "AliFOCALTrigger.h"

class TClonesArray;
class TFile;
class TObjArray;
class TParticle;
class TTree;
class TNtuple;
class TBranch;

class AliLoader;
class AliRunLoader;
class AliRun;
class AliDetector;
class AliFOCALhit;
class AliHit;
class AliHeader;
class AliCDBManager;
class AliCDBStorage;
class AliFOCALCalibData;
class AliFOCALPedestal;
class AliFOCALsdigit;
class AliFOCALdigit;
class AliFOCALGeometry;
class AliFOCALSegmentMap;


class AliFOCALDigitizer:public AliDigitizer
{
 // Methods
 public:

  AliFOCALDigitizer();
  AliFOCALDigitizer(const char * pathToGalice, char * geometryFile, Option_t *option);
  AliFOCALDigitizer(const char * pathToGalice, char * geometryFile, const char * outputFile, Option_t *option);
  AliFOCALDigitizer(const AliFOCALDigitizer &digitizer);  // copy constructor
  AliFOCALDigitizer &operator=(const AliFOCALDigitizer &digitizer); // assign op
  AliFOCALDigitizer(AliDigitizationInput *manager);
  virtual ~AliFOCALDigitizer();
  
  virtual void Digitize(Option_t* option);

  void Initialize(Option_t * option);
  void Initialize(const char * pathToGalice, const char * geometryFile, Option_t *option);
  void InitializeLocal(const char * pathToGalice, const char * geometryFile, const char * outputFile, Option_t *option);
  void Hits2DigitsEvent(Int_t ievt);
  void Hits2Digits(TBranch * branchH);
  void Hits2DigitsEmbedding(TBranch * branchHits, TBranch * branchBackground);
  void Hits2DigitsPadsPixelsOrEmbedding(TBranch * branchHits, TBranch * branchBackground);
  void Hits2DigitsEmbeddingPads(TBranch * branchHits, TBranch * branchBackground);
  void StoreDigits();
  
  bool GetDoDigits() const {return fDoDigits;}
  bool GetDoSDigits() const {return fDoSDigits;}
  bool GetLocalMode() const {return fLocalMode;}
  TClonesArray * GetDigits() const {return fDigits;}
  TClonesArray * GetSegmentDigits() const;
  TClonesArray * GetSDigits() const {return fSDigits;}
  AliFOCALSegmentMap * GetSegmentMap() const {return fSegmentMap;}
  AliFOCALSegmentMap * GetSSegmentMap() const {return fSSegmentMap;}
  TFile * GetOutputFile() const {return fOutputFile;}
  TTree * GetTreeS() const {return fTreeS;}
  TTree * GetTreeD() const {return fTreeD;}
  Int_t GetNDigit() const {return fNDigit;}
  Int_t GetNSDigit() const {return fNSDigit;}
  bool GetEmbeddOnlyPixels() const {return fEmbeddOnlyPixels;}
  bool GetSkipPixelDigits() const {return fSkipPixelDigits;}
  Int_t GetPixelGridMask() const {return fPixelGridMask;}
  bool GetRunTrigger() const {return fTriggerType;}

  void SetGeometry(AliFOCALGeometry * geom) {
    fGeom = geom;
    if (fRunPadTrigger && fTrigger != nullptr) {
      fTrigger->SetGeometry(fGeom);
    }
  }
  void SetOutputFile(TFile * outputFile) {fOutputFile = outputFile;}
  void SetOption(Option_t * option);
  void SetPadNoiseSigma(float Noise){fPadNoiseSigma = Noise;}
  void SetPixelNoiseProb(float Noise){fPixelNoiseProb = Noise;}
  void SetEmbeddOnlyPixels(bool option=true) {fEmbeddOnlyPixels = option;}
  void SetSkipPixelDigits(bool option=true) {fSkipPixelDigits = option;}
  void SetPixelGridMask(Int_t gridMask) {fPixelGridMask = gridMask;}
  void SetRunTrigger(AliFOCALTrigger::TriggerType trigType) {
    fRunPadTrigger = true;
    fTriggerType = trigType;
    fTrigger = new AliFOCALTrigger();
    if (fGeom != nullptr) {
      fTrigger->SetGeometry(fGeom);
    }
    if (trigType == AliFOCALTrigger::kLocalFull || trigType == AliFOCALTrigger::kLocal1) {
      fTrigger->SetLocalTrigger(trigType);
    }
  }
  void SetPadTriggerThreshold(double value) {
    fPadTriggerThreshold = value;
  }
 
 protected:
 
  void InitializeArraysAndMaps();
  AliFOCALdigit * GetDigit(Int_t col, Int_t row, Int_t layer);
  AliFOCALdigit * GetSDigit(Int_t col, Int_t row, Int_t layer);
  Bool_t RemoveDigit(AliFOCALdigit * digit);
  Bool_t RemoveSDigit(AliFOCALdigit * digit);
  Int_t EnergyToAmplitude(Float_t energy, Int_t segment);
  Int_t EnergyToSAmplitude(Float_t energy, Int_t segment);
  void ResetDigit();
  void ResetSDigit();
  
 // Fields
 protected:
  AliRunLoader *fRunLoader;  //! Pointer to Run Loader
  AliDetector  *fFOCAL;        //! Get pointers to Alice detectors
                             // and Hits containers 
  AliLoader    *fFOCALLoader;  //! Pointer to specific detector loader
  AliFOCALGeometry * fGeom;      // Focal Geometry

  AliFOCALTrigger* fTrigger;   // Focal pad based trigger computing utility class
  bool fRunPadTrigger;         // whether to run the pad trigger
  AliFOCALTrigger::TriggerType fTriggerType;    // trigger type
  double fPadTriggerThreshold;
  
  bool fDoDigits;             // Are we doing digits?
  bool fDoSDigits;            // Are we doing sdigits?
  bool fLocalMode;            // local mode - no modifications to galice.root
  bool fEmbeddOnlyPixels;     // If true, for the embedding option, only the pixel hits will be digitized (this option can b eused to simulate pileup, which will affect only pixels) 
  bool fSkipPixelDigits;      // If true, do not produce pixel digits
  Int_t fPixelGridMask;       // grid mask e.g. 4 means skip every 4th pixel row or column
  
  TClonesArray *fSDigits;    //! List of summable digits
  TClonesArray *fDigits;     //! List of digits
  
  AliFOCALSegmentMap * fSegmentMap;
  AliFOCALSegmentMap * fSSegmentMap;
  
  TFile * fOutputFile;        // Output file for trees (for local initialization)
  TTree * fTreeS;             // Summable digits tree (for local initialization)
  TTree * fTreeD;             // Digits tree (for local initialization)

  Int_t   fNSDigit;          // Summable digits counter
  Int_t   fNDigit;           // Digits counter
  Float_t fPixelNoiseProb;   // Noise probability per pixel
  Float_t  fPadNoiseSigma;   // Width of gaussian noise for pads
  ClassDef(AliFOCALDigitizer,11) ;   // To digitize FOCAL Hits
};

class AliFOCALSegmentMap {

 public:
 
  AliFOCALSegmentMap(): 
    fRows(0),
    fCols(0),
    fLayer(0)
  {
    fDigitMap = 0;
  }
 
  AliFOCALSegmentMap(Int_t layer, Int_t cols, Int_t rows): 
    fRows(rows),
    fCols(cols),
    fLayer(layer)
  {
    fDigitMap = new AliFOCALdigit*[fCols*fRows];
    for (Long_t i = 0; i < fCols*fRows ; i++) {
      fDigitMap[i] = 0;
    }
  }
  
  AliFOCALSegmentMap& operator=(const AliFOCALSegmentMap &fMap)
  {
    if(this!=&fMap){
      fRows = fMap.fRows;
      fCols = fMap.fCols;
      fLayer = fMap.fLayer;
      fDigitMap = new AliFOCALdigit*[fCols*fRows];
      for (Long_t i = 0; i < fCols*fRows ; i++) {
        fDigitMap[i] = fMap.fDigitMap[i];
      }
    } 
    return *this;
  }
  
  ~AliFOCALSegmentMap() {
    delete [] fDigitMap;
  }
  
  void CreateMap(Int_t layer, Int_t nCol, Int_t nRow) {
    fLayer = layer;
    fCols = nCol;
    fRows = nRow;
    fDigitMap = new AliFOCALdigit*[nCol*nRow];
    for (Long_t i = 0; i < fCols*fRows ; i++) {
      fDigitMap[i] = 0;
    }
  }
  
  void ResetMap() {
    for (Long_t i = 0; i < fCols*fRows ; i++) {
      fDigitMap[i] = 0;
    }
  }
 
  Int_t fRows;
  Int_t fCols;
  Int_t fLayer;
  AliFOCALdigit** fDigitMap;
};

#endif

