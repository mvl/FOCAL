/**************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

///////////////////////////////////////////////////////////////////////////////
//
//                                                                           //
//  Forward Calorimeter Detector                                             //
//  Functions specific to one particular geometry are              //
//  contained in the derived classes                                         //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include <TBRIK.h>
#include <TClonesArray.h>
#include <TGeometry.h>
#include <TNode.h>
#include <TTree.h>
#include <TVirtualMC.h>

#include "AliLog.h"
#include "AliLoader.h" 
#include "AliFOCALLoader.h" 
#include "AliFOCAL.h"
#include "AliRun.h"
#include "AliMC.h"
#include "AliFOCALDigitizer.h"
#include "AliFOCALhit.h"
#include "AliFOCALGeometry.h"
//#include "AliFOCALDDLRawData.h"
//#include "AliFOCALRawToSDigits.h"
  
ClassImp(AliFOCAL)
 
using namespace std;

//_____________________________________________________________________________
AliFOCAL::AliFOCAL()
  :  AliDetector(),
     fGeom(0)
{
  //
  // Default constructor
  //
  fIshunt = 0;

}
 
//_____________________________________________________________________________
AliFOCAL::AliFOCAL(const char *name, const char *title)
  : AliDetector(name,title),
    fGeom(0)
{
  //
  // Default constructor
  //

  // 
  // Allocate the array of hits
  fHits   = new TClonesArray("AliFOCALhit",  405);
  gAlice->GetMCApp()->AddHitList(fHits);

}

AliLoader* AliFOCAL::MakeLoader(const char* topfoldername)
{
  // Makes FOCAL Loader
  cout<<" AliFOCAL::MakeLoader "<<endl;
  fLoader = new AliFOCALLoader(GetName(),topfoldername);
 
  if (fLoader)
    {
      AliDebug(100,"Success");
    }
  else
    {
      AliError("Failure");
    }

  return fLoader;
}

AliFOCAL::~AliFOCAL()
{
  //
  // Destructor
  //
  //cout<<"destructor "<<endl;
  if(fGeom){
    delete fGeom;
    fGeom = 0;
  }
  //cout<<"destructor end "<<endl;

}

//_____________________________________________________________________________
void AliFOCAL::AddHit(Int_t track, Int_t *vol, Float_t *hits)
{
  //
  // Add a FOCAL hit
  //

  /// Add all of the hits even they share the same cell
  /// time difference between hits in same cell affect
  /// pulse shape and its digitizer
  //cout<<" AliFOCAL::AddHit "<<vol[0]<<" "<<vol[1]<<" "<<vol[2]<<endl;

  TClonesArray &lhits = *fHits;
  AliFOCALhit *newcell, *curcell;


  newcell = new AliFOCALhit(fIshunt, track, vol, hits);
  //cout<<newcell->GetVolume(0)<<" "<<newcell->GetVolume(1)<<" "<<newcell->GetVolume(2)<<endl;
  Int_t i;
  for (i=0; i<fNhits; i++) {
    //
    // See if this cell has already been hit
    curcell=(AliFOCALhit*) lhits[i];
    if(curcell->GetVolume(0) == newcell->GetVolume(0) && 
       curcell->GetVolume(1) == newcell->GetVolume(1) && 
       curcell->GetVolume(2) == newcell->GetVolume(2) &&
       curcell->GetVolume(3) == newcell->GetVolume(3) 
       ){

      *curcell = *curcell+*newcell;
      delete newcell;
      return;
    }
  }

  
  new(lhits[fNhits++]) AliFOCALhit(newcell);
  delete newcell;

  
  //  newcell = new(lhits[fNhits++]) AliFOCALhit(fIshunt, track, vol, hits);

}
 
//_____________________________________________________________________________
void AliFOCAL::StepManager()
{
  //
  // Called at every step in FOCAL
  //
}

//_____________________________________________________________________________
void AliFOCAL::MakeBranch(Option_t* option)
{
    // Create Tree branches for the FOCAL
    
  const char *cH = strstr(option,"H");
  if (cH && fLoader->TreeH() && (fHits == 0x0))
    fHits   = new TClonesArray("AliFOCALhit",  405);
  
  AliDetector::MakeBranch(option);
}

//_____________________________________________________________________________
void AliFOCAL::SetTreeAddress()
{
  // Set branch address

  if (fLoader->TreeH() && fHits==0x0)
    fHits   = new TClonesArray("AliFOCALhit",  405);
  
  AliDetector::SetTreeAddress();
}

//_____________________________________________________________________________
void AliFOCAL::Hits2SDigits()  
{ 
// create summable digits

  AliRunLoader* runLoader = fLoader->GetRunLoader(); 
  AliFOCALDigitizer* focalDigitizer = new AliFOCALDigitizer;
  focalDigitizer->Initialize(fLoader->GetRunLoader()->GetFileName().Data(),"","HS");

  for (Int_t iEvent = 0; iEvent < runLoader->GetNumberOfEvents(); iEvent++) {
    focalDigitizer->Hits2DigitsEvent(iEvent);
  }
  fLoader->UnloadHits();
  fLoader->UnloadSDigits();
  delete focalDigitizer;

}

//____________________________________________________________________________
void AliFOCAL::SDigits2Digits()  
{ 
  // creates sdigits to digits
}
//____________________________________________________________________________
void AliFOCAL::Hits2Digits()  
{ 
// create digits
  printf("------------- AliFOCAL::Hits2Digits \n");

  AliRunLoader* runLoader = fLoader->GetRunLoader(); 
  AliFOCALDigitizer* focalDigitizer = new AliFOCALDigitizer;
  focalDigitizer->Initialize(fLoader->GetRunLoader()->GetFileName().Data(),"","HD");


  for (Int_t iEvent = 0; iEvent < runLoader->GetNumberOfEvents(); iEvent++) {
    focalDigitizer->Hits2DigitsEvent(iEvent);
  }
  fLoader->UnloadHits();
  fLoader->UnloadDigits();
  delete focalDigitizer;

}
// ---------------------------------------------------------------------------
AliDigitizer* AliFOCAL::CreateDigitizer(AliDigitizationInput* manager) const
{ 
  return new AliFOCALDigitizer(manager);
}
// ---------------------------------------------------------------------------
void AliFOCAL::Digits2Raw()
{ 
// convert digits of the current event to raw data
/*
  fLoader->LoadDigits();
  TTree* digits = fLoader->TreeD();
  if (!digits) {
    AliError("No digits tree");
    return;
  }

  AliFOCALDDLRawData rawWriter;
  rawWriter.WriteFOCALRawData(digits);

  fLoader->UnloadDigits();
*/
}


Bool_t AliFOCAL::Raw2SDigits(AliRawReader * /*rawReader*/)
{
  /*
  // converts raw to sdigits
  AliRunLoader* runLoader = fLoader->GetRunLoader(); 
  //runLoader->GetEvent(ievt);

  AliFOCALRawToSDigits focalr2sd;
  focalr2sd.Raw2SDigits(runLoader, rawReader);
  fLoader->UnloadSDigits();
  */
  return kTRUE;
}

Bool_t AliFOCAL::IsInFOCAL(Double_t x, Double_t y, Double_t z) {
  //
  //  Check whether a coordinate is within the FOCAL volume
  //    NB: the hole around the beampipe is counted as inside
  //  Also note that the FOCAL dimensions are cached on the first call
  //

  // Cache dimensions of FOCAL for performance
  static Double_t hsizeZ = fGeom->GetFOCALSizeZ()/2.; 
  static Double_t hsizeX = fGeom->GetFOCALSizeX()/2.; 
  static Double_t hsizeY = fGeom->GetFOCALSizeY()/2.; 

  if (TMath::Abs(z - fGeom->GetFOCALZ0()) > hsizeZ)
    return kFALSE;
  if (TMath::Abs(x) > hsizeX)
    return kFALSE;
  if (TMath::Abs(y) > hsizeY)
    return kFALSE;

  return kTRUE;
}
