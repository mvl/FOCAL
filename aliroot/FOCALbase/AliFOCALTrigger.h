#ifndef ALIFOCALTRIGGER_H
#define ALIFOCALTRIGGER_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */
//-----------------------------------------------------//
//                                                     //
//  Date   : November 11 2022                          //
//                                                     //
//  Compute trigger for FOCAL                          //
//                                                     //
//-----------------------------------------------------//

// contact: Ionut Arsene, iarsene@cern.ch

#include "TObject.h"
#include "AliFOCALGeometry.h"


#include <vector>

class TClonesArray;

class AliFOCALTrigger : public TObject
{
 public:

  enum TriggerType {
    kNoTrigger = -1,
    kLocalFull = 0,
    kLocal1,
    kGlobalInner,
    kGlobal,
    kNTriggers
  };

  AliFOCALTrigger();
  AliFOCALTrigger (const AliFOCALTrigger &other);  // copy constructor
  AliFOCALTrigger &operator=(const AliFOCALTrigger &other); // assignment op
  virtual ~AliFOCALTrigger();
  
  void SetGeometry(AliFOCALGeometry* geom) {fGeom = geom;}
  void SetPadRegionThreshold(double value) {fPadRegionThreshold = value;}
  void SetLocalTrigger(TriggerType value) {fLocalTrigger = value;}
  double GetPadRegionThreshold() const {return fPadRegionThreshold;}
  TriggerType GetLocalTrigger() const {return fLocalTrigger;}
  bool IsTriggerFired(TriggerType itrig) {
    return fComputedTriggers[itrig];
  }
  std::map<int, bool> GetFiredPixelSensors() {
    return fFiredPixelSensors;
  }
  
  void BuildLUTs();
  void BuildLUTLocalFull();
  void BuildLUTLocal1();
  void ComputeTriggers(TClonesArray* digits);
  void PrintPixSensors(int triggerZone);
  auto GetPixSensors(int triggerZone) {
    return fPadToPixel[triggerZone];
  }
    
 protected:
  
  AliFOCALGeometry* fGeom;    // Focal geometry
  bool fComputedTriggers[kNTriggers];  // computed triggers
  double fPadRegionThreshold;       // low amplitude threshold for a pad trigger region

  std::map<int, bool> fFiredPixelSensors;                            // key: pixel sensor index; value: true-fired, false-not fired
  
  // look-up tables needed for trigger computation
  std::map<std::pair<int, int>, int> fPadTriggerZones;               // key: pad sensor (col,row); value: pad trigger zone index
  std::map<int, std::vector<int>> fPadToPixel;                       // key: pad trigger zone index; value: vector of overlaping pixel sensor indices
  //std::map<int, std::tuple<int, int, int, int>> fPixelSensorRanges;  // key: pixel sensor index; value: (low_col, low_row, high_col, high_row)
  TriggerType fLocalTrigger;                                         // local trigger type

  int GetPixelSensor(int iCol, int iRow, int padWaferCol, int padWaferRow, int nRowPix);
  
  ClassDef(AliFOCALTrigger,1) // Trigger decision for FOCAL
};

#endif
