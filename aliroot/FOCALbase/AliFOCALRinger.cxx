#include "AliFOCALRinger.h"
#include "TMath.h"
#include "Riostream.h"

using namespace std;

AliFOCALRinger::AliFOCALRinger():
fPosition(0),
fRadius(0),
fMaxRadius(0),
fRingCoordinates(0),
fRingSizes(0)
{
}

AliFOCALRinger::AliFOCALRinger(Int_t maxRadius):
fPosition(0),
fRadius(0),
fMaxRadius(0),
fRingCoordinates(0),
fRingSizes(0)
{
  Init(maxRadius);
}

// Default destructor
AliFOCALRinger::~AliFOCALRinger() {
  
  if (fRingCoordinates) {
    for (Int_t radius = 1 ; radius <= fMaxRadius ; radius++)
      delete [] fRingCoordinates[radius-1];
      
    delete [] fRingCoordinates;
  }
  
  if (fRingSizes)
    delete [] fRingSizes;
}

// Calculates sequential coordinates x,y for rings from 1 to maxRadius and stores them in arrays.
bool AliFOCALRinger::Init(Int_t maxRadius) {

  
  if (fRingCoordinates) {
    for (Int_t radius = 1 ; radius <= fMaxRadius ; radius++)
      delete [] fRingCoordinates[radius-1];
      
    delete [] fRingCoordinates;
  }
  
  if (fRingSizes)
    delete [] fRingSizes;
    
    
  fMaxRadius = maxRadius;
  fRingSizes = new Int_t[fMaxRadius];
  fRingCoordinates = new Int_t * [fMaxRadius];
  
  
  // First loop to find out sizes of individual rings...
  for (Int_t radius = 1 ; radius <= fMaxRadius ; radius++) {
    Int_t size = 0;
    Int_t y_min, y_max;
    for (Int_t x = -radius; x <= radius ; x++ ) {
      y_max = (TMath::Sqrt((radius+0.5)*(radius+0.5) - x*x)); 
      if (TMath::Abs(x) < radius) 
        y_min = (TMath::Sqrt((radius-0.5)*(radius-0.5) - x*x))+1; 
      else 
        y_min = 0; 
      if (y_min > y_max) y_min = y_max; 
//      cout << y_min << "," << y_max << endl; 
      for (Int_t y = y_min ; y <= y_max ; y++) { 
        size++;
        if (y != 0) 
          size++;
      }
    }
    fRingSizes[radius-1] = size;
  }
  
  
  for (Int_t radius = 1 ; radius <= fMaxRadius ; radius++) {
    fRingCoordinates[radius-1] = new Int_t[fRingSizes[radius-1]*2]; 
  }
  
  
  // Second loop to fill arrays...
  for (Int_t radius = 1 ; radius <= fMaxRadius ; radius++) {
    Int_t pos = 0;
    Int_t y_min, y_max;
    for (Int_t x = -radius; x <= radius ; x++ ) {
      y_max = (TMath::Sqrt((radius+0.5)*(radius+0.5) - x*x)); 
      if (TMath::Abs(x) < radius) 
        y_min = (TMath::Sqrt((radius-0.5)*(radius-0.5) - x*x))+1; 
      else 
        y_min = 0;
      if (y_min > y_max) y_min = y_max; 
//      cout << y_min << "," << y_max << endl; 
      for (Int_t y = y_min ; y <= y_max ; y++) { 
        fRingCoordinates[radius-1][pos++] = x;
        fRingCoordinates[radius-1][pos++] = y;
        if (y != 0) {
           fRingCoordinates[radius-1][pos++] = x;
           fRingCoordinates[radius-1][pos++] = -y;
        }
      }
    }
  }
  
  return true;
}

bool AliFOCALRinger::SetRing(Int_t radius) {
  
  if (radius > fMaxRadius)
    return false;
  
  fRadius = radius;
  fPosition = 0;
  
  return true;
}

bool AliFOCALRinger::GetNext(Int_t & x, Int_t & y) {

  if (fRadius == 0) {
    if (fPosition != 0)
      return false;
    else {
      x = 0;
      y = 0;
      fPosition++;
      return true;
    }
  }

  if (fPosition >= 2*fRingSizes[fRadius-1])
    return false;
    
  x = fRingCoordinates[fRadius-1][fPosition++];
  y = fRingCoordinates[fRadius-1][fPosition++];
  
  return true;
}

