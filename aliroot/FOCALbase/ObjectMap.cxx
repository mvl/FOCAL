#include "ObjectMap.h"

ObjectMap::ObjectMap(): 
  fRows(0),
  fCols(0)
{
  fObjectMap = 0;
}

ObjectMap::ObjectMap(Int_t cols, Int_t rows): 
  fRows(rows),
  fCols(cols)
{
  fObjectMap = new TObject * [fCols*fRows];
  for (Long_t i = 0; i < fCols*fRows ; i++) {
    fObjectMap[i] = 0;
  }
}

ObjectMap& ObjectMap::operator=(const ObjectMap &fMap)
{
  if(this!=&fMap){
    fRows = fMap.fRows;
    fCols = fMap.fCols;
    fObjectMap = new TObject * [fCols*fRows];
    for (Long_t i = 0; i < fCols*fRows ; i++) {
      fObjectMap[i] = fMap.fObjectMap[i];
    }
  } 
  return *this;
}

ObjectMap::~ObjectMap() {
  delete [] fObjectMap;
}

void ObjectMap::CreateMap(Int_t nCol, Int_t nRow) {
  
  fCols = nCol;
  fRows = nRow;
  
  fObjectMap = new TObject * [fCols*fRows];
  for (Long_t i = 0; i < fCols*fRows ; i++) {
    fObjectMap[i] = 0;
  }
}

void ObjectMap::ResetMap() {

  for (Long_t i = 0; i < fCols*fRows ; i++) {
    fObjectMap[i] = 0;
  }
}

TObject * ObjectMap::Get(Int_t col, Int_t row) {
  
  if (!fObjectMap)
    return 0;
  
  if ((col < 0) || (col >= fCols) || (row < 0) || (row >= fRows))
    return 0;
  
  return fObjectMap[row*fCols + col];
}

Bool_t ObjectMap::InsertAt(Int_t col, Int_t row, TObject * obj) {
  
  if (!fObjectMap)
    return false;
  
  if ((col < 0) || (col >= fCols) || (row < 0) || (row >= fRows))
    return false;
  
  fObjectMap[row*fCols + col] = obj;
  return true;
}

