#ifndef ALIFOCALGETTER_H
#define ALIFOCALGETTER_H

//base class for loaders 
//loader is common for reading data for all detectors
//Each detector has a loader data member
//loader is accessible via folder structure as well

#include <AliLoader.h>



class AliFOCALLoader: public AliLoader
 {
 public:
   AliFOCALLoader();
   AliFOCALLoader(const Char_t *name,const Char_t *topfoldername);
   AliFOCALLoader(const Char_t *name,TFolder *topfolder);
   
   virtual ~AliFOCALLoader(){};//-----------------
   
 protected:
   
   
 private:
   static const TString fgkDefaultHitsFileName;
   static const TString fgkDefaultSDigitsFileName;
   static const TString fgkDefaultDigitsFileName;
   static const TString fgkDefaultRecPointsFileName;
   static const TString fgkDefaultTracksFileName;
   
   
 public:
   ClassDef(AliFOCALLoader,1);
 };
 
#endif
