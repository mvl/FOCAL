////////////////
// Macro for drawing the direct photon efficiency
////////////////

#include "legend_utils.C"

void makePlotsPythiaCombined()
{

  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  Int_t lw = 2;
  gStyle->SetLineWidth(lw);
  gStyle->SetFrameLineWidth(lw);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetNdivisions(505, "Y");
  set_legend_font(42, 0.035);

  gStyle->SetTitleOffset(1.2, "Y");
  gStyle->SetTitleOffset(1.2, "X");

  const Bool_t draw_pysa = 0;   // Draw Pythia standalone results for comparison
  const Bool_t alice_label = 1; // Draw 'ALICE simlation' label

  const Int_t nptbins = 16;
  Float_t ptbins[nptbins + 1] = {2.0, 2.5, 3.0, 3.5, 4.0,
                                 4.5, 5.0, 5.5, 6.0, 6.5,
                                 7.0, 8.0, 9.0, 10.0, 11.0,
                                 13.0, 15.0};

  const Int_t splitImassShowShape = 1;
  // Char_t *label1 = "pp #sqrt{s} = 14 TeV";

  // Old LoI 4m position
  // Char_t *label2 = "High Granularity, no HCAL";
  // TFile *finmb = new TFile("data/analysis_pythia_config62_ceneknew_all_shapes.root");
  // TFile *findg = new TFile("data/analysis_pythia_config7_ceneknew_all.root");

  /*
  Char_t *label2 = "HG, no HCAL, 8m";
  TFile *finmb = new TFile("analysis_pythia_8m_mb_ceneknew_all.root");
  TFile *findg = new TFile("analysis_pythia_8m_dg_ceneknew_all.root");
  */

  // New LoI 7m position (with HCAL)
  /*
  const Char_t *label1 = "pp #sqrt{s} = 13 TeV";
  const Char_t *label2 = "7m position";
  const Float_t minPt = 4; // GeV
  const Float_t gdir_leg_ypos = 0.15;
  const Float_t gdir_label_ypos = 0.75;
  TFile *finmb = new TFile("data_loi2014/analysis_pythia_MBtrig_13tev_loi2014_all.root");
  //TFile *finmb = new TFile("data_loi2014/analysis_pythia_MBEMdecaytrig_13tev_loi2014_all.root");
  //TFile *finmb = new TFile("data_loi2014/analysis_pythia_MBEMdecaytrig_13tev_new_loi2014_all.root");
  //TFile *finmb = new TFile("data_loi2014/analysis_pythia_MBEMtrig_13tev_loi2014_all.root");
  TFile *findg = new TFile("data_loi2014/analysis_pythia_dirgamma_13tev_loi2014_all.root");
  */

  // 14 TeV (from quark)
  const Char_t *label1 = "pp #sqrt{s} = 14 TeV";
  const Float_t minPt = 4; // GeV
  const Float_t gdir_leg_ypos = 0.22;
  const Float_t gdir_label_ypos = 0.75;
  /*
  const Char_t *label2 = "7m position";
  TFile *finmb = new TFile("data_quark/analysis_pythia_MBtrig_loi_all.root");
  cout << "WARNING: Should doubble-check whether dirgamma process was used here !!! " << endl;
  TFile *findg = new TFile("data_quark/analysis_pythia_dirgam_loi_all.root");
  */
  const Char_t *label2 = "with FIT";
  TFile *finmb = new TFile("data_quark/analysis_pythia_MBtrig_wFIT_newpars_all.root");
  TFile *findg = new TFile("data_quark/analysis_pythia_dirgam_wFIT_newpars_all.root");

  /*
  const Char_t *label1 = "HIJING p+Pb #sqrt{s} = 8.16 TeV";
  const Char_t *label2 = "7m position";
  const Float_t minPt = 5;  // GeV
  const Float_t gdir_leg_ypos = 0.15; //0.75;
  const Float_t gdir_label_ypos = 0.75;
  TFile *finmb = new TFile("data_loi2014/analysis_hijing_PA_MBEMdecaytrig_loi2014_all.root");
  TFile *findg = new TFile("data_loi2014/analysis_hijing_PA_directgamma_loi2014_all.root");
  */

  // 9m position, 'rapidity 6'
  /*
  const Char_t *label1 = "pp #sqrt{s} = 14 TeV";
  const Char_t *label2 = "9m position";
  const Float_t minPt = 4; // GeV
  const Float_t gdir_leg_ypos = 0.15;
  const Float_t gdir_label_ypos = 0.75;
  TFile *finmb = new TFile("data_rap6/analysis_rap6_pythia_MBtrig_loi_all.root"); // 14 TeV
  TFile *findg = new TFile("data_rap6/analysis_rap6_pythia_dirgam_loi_all.root"); // 14 TeV
  */

  cout << "MB file: " << finmb->GetName() << endl;
  cout << "direct gamma file: " << findg->GetName() << endl;
  TTree *clustTree_mb = (TTree *)finmb->Get("clustTree");

  TTree *clustTree_dg = (TTree *)findg->Get("clustTree");

  TFile *fin_mbsa = 0;
  TFile *fin_dgsa = 0;
  if (draw_pysa)
  {
    fin_mbsa = new TFile("~/Dropbox/focal/focal_plots/pythia_hadr_14tev.root"); // Pythia standalone
    fin_dgsa = new TFile("~/Dropbox/focal/focal_plots/pythia_phot_14tev.root"); // Pythia standalone
  }

  TProfile *xsec_h = (TProfile *)finmb->Get("xsec_h");
  TH1D *trials_h = (TH1D *)finmb->Get("trials_h");

  Float_t xsec_mb = 0;
  Float_t ntrials_mb = 0;
  Float_t scale_mb = 1;

  if (xsec_h)
  { // No xsec info in Hijing
    xsec_mb = xsec_h->GetBinContent(1);
    ntrials_mb = trials_h->GetBinContent(1);
    if (ntrials_mb != 0)
      scale_mb = xsec_mb / ntrials_mb;
  }

  xsec_h = (TProfile *)findg->Get("xsec_h");
  trials_h = (TH1D *)findg->Get("trials_h");

  Float_t xsec_dg = 0;
  Float_t ntrials_dg = 0;
  Float_t scale_dg = 1;
  if (xsec_h)
  {
    xsec_dg = xsec_h->GetBinContent(1);
    ntrials_dg = trials_h->GetBinContent(1);
    if (ntrials_dg != 0)
      scale_dg = xsec_dg / ntrials_dg;
  }

  cout << " MB events, trials: " << ntrials_mb << " xsec " << xsec_mb << endl;
  cout << " gamma events, trials: " << ntrials_dg << " xsec " << xsec_dg << endl;

  Float_t eta_min = 4.75; // 4.0; // 3.0; //4.0; //3.0;
  Float_t eta_max = 5.25; // 5.0; // 4.0; //5.0; //4.0;

  TCut etacut(Form("etaclust>%.1f&&etaclust<%.1f", eta_min, eta_max));
  TString etalabel(Form("%.2f < #eta < %.2f", eta_min, eta_max));
  TCut imasscut("(imass_pi0<0.07||imass_pi0>0.18)");
  TCut dirgammasel("pdg_match==22&&(pdg_parent_match==22||TMath::Abs(pdg_parent_match)<10)&&dist_match<0.6");

  TCut shapecut("W1Cclust<0.8&&W2Cclust<0.7&&ERclust<0.3");
  // 20190707: (longitudinal) energy ratio cut is not very selective; try without
  // TCut shapecut("W1Cclust<0.8&&W2Cclust<0.6");
  // TCut shapecut("");

  // TCut isocut("ptisoR4<1.0"); //0.5");
  // TString isolabel("#it{R}_{iso}=0.4, #it{p}_{T,iso} < 1.0 GeV"); //0.5 GeV");

  static const Float_t scaleHCAL = 1. / 10300.; // 4620. was used in reco
  // TCut isocut(Form("ptisoR4EMMC-pt_match+ptisoR4HadMC < 0.5",scaleHCAL)); //0.5");
  // TString isolabel("#it{R}_{iso}=0.4, #it{p}_{T,iso}^{MC} < 0.5 GeV"); //0.5 GeV");
  //  pp EM+HCAL
  TCut isocut(Form("ptisoR4+%f*ptisoR4HCAL < 3.0", scaleHCAL));         // 0.5");
  TString isolabel("#it{R}_{iso}=0.4, #it{p}_{T,iso}^{E+H} < 3.0 GeV"); // 0.5 GeV");
  // rap 5.5
  // TCut isocut(Form("ptisoR4+%f*ptisoR4HCAL < 2.0",scaleHCAL));
  // TString isolabel("#it{R}_{iso}=0.4, #it{p}_{T,iso}^{E+H} < 2.0 GeV");
  // rap 6
  // TCut isocut(Form("ptisoR4+%f*ptisoR4HCAL < 1.0",scaleHCAL));
  // TString isolabel("#it{R}_{iso}=0.4, #it{p}_{T,iso}^{E+H} < 1.0 GeV");
  // pPb EM+HCAL
  // TCut isocut(Form("ptisoR4+%f*ptisoR4HCAL < 5.0",scaleHCAL)); //0.5");
  // TString isolabel("#it{R}_{iso}=0.4, #it{p}_{T,iso}^{E+H} < 5.0 GeV"); //0.5 G

  // TCut isocut("ptisoR2<0.5");
  // TString isolabel("isolation R=0.2, p_{T} < 0.5 GeV");

  TCanvas *c1 = new TCanvas("c1", "c1: pt spec", 1200, 600);
  c1->Divide(2, 1);
  c1->cd(1);
  gPad->SetLogy();

  TH1D *ptclust_mb = new TH1D("ptclust_mb", "cluster distr MB events;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptclust_dg = new TH1D("ptclust_dg", "cluster distr DG events;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptclust_dgsel = new TH1D("ptclust_dgsel", "matched cluster distr DG events;p_{T} (GeV)", nptbins, ptbins);

  clustTree_mb->Draw("ptclust>>ptclust_mb", etacut, "");
  clustTree_dg->Draw("ptclust>>ptclust_dg", etacut, "same");
  clustTree_dg->Draw("ptclust>>ptclust_dgsel", etacut && dirgammasel, "same");

  // ptclust_mb = (TH1*) gDirectory->Get("ptclust_mb");
  ptclust_mb->Sumw2();
  ptclust_mb->SetXTitle("p_{T,clust} (GeV)");
  ptclust_mb->SetMarkerStyle(20);
  ptclust_mb->SetMarkerColor(4);
  ptclust_mb->SetLineColor(4);
  ptclust_mb->SetLineWidth(lw);
  ptclust_mb->Scale(scale_mb, "width");
  ptclust_mb->SetMinimum(1e-6);

  // ptclust_dg = (TH1D*) gDirectory->Get("ptclust_dg");
  ptclust_dg->Sumw2();
  ptclust_dg->SetMarkerStyle(27);
  ptclust_dg->SetMarkerColor(2);
  ptclust_dg->SetLineColor(2);
  ptclust_dg->SetLineWidth(lw);
  ptclust_dg->Scale(scale_dg, "width");

  // ptclust_dgsel = (TH1D*) gDirectory->Get("ptclust_dgsel");
  ptclust_dgsel->Sumw2();
  ptclust_dgsel->SetMarkerStyle(20);
  ptclust_dgsel->SetMarkerColor(2);
  ptclust_dgsel->SetLineColor(2);
  ptclust_dgsel->SetLineWidth(lw);
  ptclust_dgsel->Scale(scale_dg, "width");

  TH1D *ptcl_decrej_mb = new TH1D("ptcl_decrej_mb", "cluster distr MB events with dec rej;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptcl_decrej_dg = new TH1D("ptcl_decrej_dg", "cluster distr DG events with dec rej;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptcl_imassrej_mb = new TH1D("ptcl_imassrej_mb", "cluster distr MB events with inv mass rej;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptcl_imassrej_dg = new TH1D("ptcl_imassrej_dg", "cluster distr DG events with inv mass rej;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptcl_ssrej_mb = new TH1D("ptcl_ssrej_mb", "cluster distr MB events with show shape rej;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptcl_ssrej_dg = new TH1D("ptcl_ssrej_dg", "cluster distr DG events with show shape rej;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptcl_iso_mb = new TH1D("ptcl_iso_mb", "cluster distr MB events with isolation;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptcl_iso_dg = new TH1D("ptcl_iso_dg", "cluster distr DG events with isolation;p_{T} (GeV)", nptbins, ptbins);

  TH1D *ptcl_decrejiso_mb = new TH1D("ptcl_decrejiso_mb", "cluster distr MB events with dec rej and isolation;p_{T} (GeV)", nptbins, ptbins);
  TH1D *ptcl_decrejiso_dg = new TH1D("ptcl_decrejiso_dg", "cluster distr DG events with dec rej and isolation;p_{T} (GeV)", nptbins, ptbins);

  clustTree_dg->SetLineColor(2);
  if (splitImassShowShape)
  {
    clustTree_mb->Draw("ptclust>>ptcl_imassrej_mb", etacut && imasscut, "same");

    clustTree_dg->Draw("ptclust>>ptcl_imassrej_dg", etacut && imasscut && dirgammasel, "same");

    ptcl_imassrej_mb->Sumw2();
    ptcl_imassrej_mb->Scale(scale_mb, "width");
    ptcl_imassrej_mb->SetMarkerStyle(26);
    ptcl_imassrej_mb->SetMarkerColor(4);
    ptcl_imassrej_mb->SetLineColor(4);
    ptcl_imassrej_mb->SetLineWidth(lw);

    ptcl_imassrej_dg->Sumw2();
    ptcl_imassrej_dg->Scale(scale_dg, "width");
    ptcl_imassrej_dg->SetMarkerStyle(26);
    ptcl_imassrej_dg->SetMarkerColor(2);
    ptcl_imassrej_dg->SetLineColor(2);
    ptcl_imassrej_dg->SetLineWidth(lw);

    clustTree_mb->Draw("ptclust>>ptcl_ssrej_mb", etacut && shapecut, "same");

    clustTree_dg->Draw("ptclust>>ptcl_ssrej_dg", etacut && shapecut && dirgammasel, "same");

    ptcl_ssrej_mb->Sumw2();
    ptcl_ssrej_mb->Scale(scale_mb, "width");
    ptcl_ssrej_mb->SetMarkerStyle(24);
    ptcl_ssrej_mb->SetMarkerColor(4);
    ptcl_ssrej_mb->SetLineColor(4);
    ptcl_ssrej_mb->SetLineWidth(lw);

    ptcl_ssrej_dg->Sumw2();
    ptcl_ssrej_dg->Scale(scale_dg, "width");
    ptcl_ssrej_dg->SetMarkerStyle(24);
    ptcl_ssrej_dg->SetMarkerColor(2);
    ptcl_ssrej_dg->SetLineColor(2);
    ptcl_ssrej_dg->SetLineWidth(lw);
  }
  else
  {
    clustTree_mb->Draw("ptclust>>ptcl_decrej_mb", etacut && imasscut && shapecut, "same");

    clustTree_dg->Draw("ptclust>>ptcl_decrej_dg", etacut && imasscut && shapecut && dirgammasel, "same");

    ptcl_decrej_mb->Sumw2();
    ptcl_decrej_mb->Scale(scale_mb, "width");
    ptcl_decrej_mb->SetMarkerStyle(26);
    ptcl_decrej_mb->SetMarkerColor(4);
    ptcl_decrej_mb->SetLineColor(4);
    ptcl_decrej_mb->SetLineWidth(lw);

    ptcl_decrej_dg->Sumw2();
    ptcl_decrej_dg->Scale(scale_dg, "width");
    ptcl_decrej_dg->SetMarkerStyle(26);
    ptcl_decrej_dg->SetMarkerColor(2);
    ptcl_decrej_dg->SetLineColor(2);
    ptcl_decrej_dg->SetLineWidth(lw);
  }

  clustTree_mb->Draw("ptclust>>ptcl_iso_mb", etacut && isocut, "same");
  // clustTree_dg->Draw("ptclust>>ptcl_iso_dg",etacut&&isocut,"same");
  clustTree_dg->Draw("ptclust>>ptcl_iso_dg", etacut && isocut && dirgammasel, "same"); // 2016-Mar-09 added dirgammasel
  ptcl_iso_mb->Sumw2();
  ptcl_iso_mb->Scale(scale_mb, "width");
  ptcl_iso_mb->SetMarkerStyle(25);
  ptcl_iso_mb->SetLineColor(4);
  ptcl_iso_mb->SetLineWidth(lw);
  ptcl_iso_mb->SetMarkerColor(4);

  ptcl_iso_dg->Sumw2();
  ptcl_iso_dg->Scale(scale_dg, "width");
  ptcl_iso_dg->SetMarkerStyle(25);
  ptcl_iso_dg->SetMarkerColor(2);
  ptcl_iso_dg->SetLineColor(2);
  ptcl_iso_dg->SetLineWidth(lw);

  clustTree_mb->Draw("ptclust>>ptcl_decrejiso_mb", etacut && imasscut && shapecut && isocut, "same");
  clustTree_dg->Draw("ptclust>>ptcl_decrejiso_dg", etacut && imasscut && shapecut && isocut && dirgammasel, "same");

  gPad->Update();

  ptcl_decrejiso_mb->Sumw2();
  ptcl_decrejiso_mb->Scale(scale_mb, "width");
  ptcl_decrejiso_mb->SetMarkerStyle(21);
  ptcl_decrejiso_mb->SetMarkerColor(4);
  ptcl_decrejiso_mb->SetLineColor(4);
  ptcl_decrejiso_mb->SetLineWidth(lw);

  ptcl_decrejiso_dg->Sumw2();
  ptcl_decrejiso_dg->Scale(scale_dg, "width");
  ptcl_decrejiso_dg->SetMarkerStyle(21);
  ptcl_decrejiso_dg->SetMarkerColor(2);
  ptcl_decrejiso_dg->SetLineColor(2);
  ptcl_decrejiso_dg->SetLineWidth(lw);

  Int_t idx = 0;
  draw_legend_m(0.55, 0.9, ptclust_mb, "no selection", idx++);
  if (splitImassShowShape)
  {
    draw_legend_m(0.55, 0.9, ptcl_imassrej_mb, "inv mass rej", idx++);
    draw_legend_m(0.55, 0.9, ptcl_ssrej_mb, "show shape", idx++);
  }
  else
  {
    draw_legend_m(0.55, 0.9, ptcl_decrej_mb, "dec rej (IM+SS)", idx++);
  }
  draw_legend_m(0.55, 0.9, ptcl_iso_mb, "isolated", idx++);
  draw_legend_m(0.55, 0.9, ptcl_decrejiso_mb, "dec rej + iso", idx++);
  // draw_legend_m(0.5,0.9,ptclust_dgsel,"#gamma-dir",3);
  // draw_legend_m(0.5,0.9,ptcl_decrej_dg,"#gamma-dir dec rejected",4);
  // draw_legend_m(0.5,0.9,ptcl_decrejiso_dg,"#gamma-dir dec rej + iso",5);

  TLatex *ltx = new TLatex;
  ltx->SetNDC();
  ltx->SetTextFont(42);
  ltx->SetTextSize(0.035);
  ltx->DrawLatex(0.3, 0.85, label1);
  ltx->DrawLatex(0.3, 0.8, etalabel);
  ltx->DrawLatex(0.5, 0.15, isolabel);
  // Compare to Pythia standalone (no det sim)
  c1->cd(2);
  gPad->SetLogy();
  ptclust_mb->SetLineWidth(lw);
  ptclust_mb->Draw();
  ptclust_dgsel->SetLineWidth(lw);
  ptclust_dgsel->Draw("same");

  if (draw_pysa)
  {
    TH2F *hEtaPtMBSA = (TH2F *)fin_mbsa->Get("hEtaPtDecPhoton");
    TH2F *hEtaPtPi0MBSA = (TH2F *)fin_mbsa->Get("hEtaPtPiZero");
    TH1F *hNEventMBSA = (TH1F *)fin_mbsa->Get("hNEvent");
    TProfile *hXSecMBSA = (TProfile *)fin_mbsa->Get("hXSec");
    Float_t scale_mb_sa = hXSecMBSA->GetBinContent(1) / hNEventMBSA->GetBinContent(1);
    // cout << "Scale for pytha standalone: " << scale_py_sa << endl;
    Int_t min_bin = hEtaPtMBSA->GetXaxis()->FindBin(eta_min + 0.001);
    Int_t max_bin = hEtaPtMBSA->GetXaxis()->FindBin(eta_max - 0.001);

    cout << "eta range from Pythia SA " << hEtaPtMBSA->GetXaxis()->GetBinLowEdge(min_bin) << " - " << hEtaPtMBSA->GetXaxis()->GetBinUpEdge(max_bin) << endl;

    TH1D *hPtMBSA = hEtaPtMBSA->ProjectionY("hPtMBSA", min_bin, max_bin);
    hPtMBSA->Rebin(2);
    hPtMBSA->Scale(scale_mb_sa, "width");
    hPtMBSA->SetLineColor(4);
    hPtMBSA->Draw("same");

    TH1D *hPtPi0MBSA = hEtaPtPi0MBSA->ProjectionY("hPti0MBSA", min_bin, max_bin);
    hPtPi0MBSA->Rebin(2);
    hPtPi0MBSA->Scale(scale_mb_sa, "width");
    hPtPi0MBSA->SetLineColor(4);
    hPtPi0MBSA->SetLineStyle(4);
    hPtPi0MBSA->Draw("same");

    TH2F *hEtaPtDGSA = (TH2F *)fin_dgsa->Get("hEtaPtDirPhoton");
    TH1F *hNEventDGSA = (TH1F *)fin_dgsa->Get("hNEvent");
    TProfile *hXSecDGSA = (TProfile *)fin_dgsa->Get("hXSec");
    Float_t scale_dg_sa = hXSecDGSA->GetBinContent(1) / hNEventDGSA->GetBinContent(1);
    // cout << "Scale for pytha standalone: " << scale_py_sa << endl;

    TH1D *hPtDGSA = hEtaPtDGSA->ProjectionY("hPtDGSA", min_bin, max_bin);
    hPtDGSA->Rebin(2);
    hPtDGSA->Scale(scale_dg_sa, "width");
    hPtDGSA->SetLineColor(2);
    hPtDGSA->Draw("same");
  }

  TCanvas *c2a = new TCanvas("c2a", "c2a: bkg efficiency", 600, 600);
  c2a->cd();
  // gPad->SetLogy();
  TH1D *eff_h = 0;
  TH1D *eff_shape_h = 0;
  if (splitImassShowShape)
  {
    eff_h = (TH1D *)ptcl_imassrej_mb->Clone("eff_mb_h");
    eff_shape_h = (TH1D *)ptcl_ssrej_mb->Clone("eff_shape_mb_h");
    eff_shape_h->Divide(eff_shape_h, ptclust_mb, 1, 1, "B");
  }
  else
  {
    eff_h = (TH1D *)ptcl_decrej_mb->Clone("eff_mb_h");
    eff_h->SetMaximum(0.45);
  }
  eff_h->Divide(eff_h, ptclust_mb, 1, 1, "B");
  eff_h->SetXTitle("#it{p}_{T} (GeV/#it{c})");
  eff_h->SetYTitle("bkg efficiency");
  eff_h->GetYaxis()->SetTitleOffset(1.3);
  eff_h->SetMinimum(0.0);
  eff_h->GetXaxis()->SetRangeUser(minPt, 15);
  eff_h->Draw();

  if (splitImassShowShape)
  {
    eff_h->SetMaximum(0.75);
    eff_shape_h->Draw("same");
  }

  TH1D *eff_iso_h = (TH1D *)ptcl_iso_mb->Clone("eff_iso_mb_h");
  eff_iso_h->Divide(eff_iso_h, ptclust_mb, 1, 1, "B");
  eff_iso_h->Draw("same");

  TH1D *eff_djiso_h = (TH1D *)ptcl_decrejiso_mb->Clone("eff_djiso_mb_h");
  eff_djiso_h->Divide(eff_djiso_h, ptclust_mb, 1, 1, "B");
  eff_djiso_h->Draw("same");

  ltx->DrawLatex(0.15, 0.85, isolabel);
  ltx->DrawLatex(0.15, 0.8, etalabel);
  gPad->Update();
  idx = 0;
  if (splitImassShowShape)
  {
    draw_legend_m(0.3, 0.75, eff_h, "inv mass rej", idx++);
    draw_legend_m(0.3, 0.75, eff_shape_h, "show shape", idx++);
  }
  else
  {
    draw_legend_m(0.3, 0.75, eff_h, "dec rej (IM+SS)", idx++);
  }
  draw_legend_m(0.3, 0.75, eff_iso_h, "isolation", idx++);
  draw_legend_m(0.3, 0.75, eff_djiso_h, "dec rej + iso", idx++);

  TCanvas *c2b = new TCanvas("c2b", "c2b: dir gamma efficiency", 600, 600);
  c2b->cd();
  if (splitImassShowShape)
  {
    eff_h = (TH1D *)ptcl_imassrej_dg->Clone("eff_h");
    eff_shape_h = (TH1D *)ptcl_ssrej_dg->Clone("eff_shape_h");
    eff_shape_h->Divide(eff_shape_h, ptclust_dgsel, 1, 1, "B");
  }
  else
  {
    eff_h = (TH1D *)ptcl_decrej_dg->Clone("eff_h");
  }
  eff_h->Divide(eff_h, ptclust_dgsel, 1, 1, "B");
  eff_h->SetXTitle("#it{p}_{T} (GeV/#it{c})");
  eff_h->SetYTitle("#gamma_{dir} efficiency");
  eff_h->SetMaximum(1.1);
  eff_h->SetMinimum(0);
  eff_h->GetXaxis()->SetRangeUser(minPt, 15);
  eff_h->Draw();

  if (splitImassShowShape)
  {
    eff_shape_h->Draw("same");
  }

  eff_iso_h = (TH1D *)ptcl_iso_dg->Clone("eff_iso_h");
  eff_iso_h->Divide(eff_iso_h, ptclust_dgsel, 1, 1, "B");
  eff_iso_h->Draw("same");

  eff_djiso_h = (TH1D *)ptcl_decrejiso_dg->Clone("eff_djiso_h");
  eff_djiso_h->Divide(eff_djiso_h, ptclust_dgsel, 1, 1, "B");
  eff_djiso_h->Draw("same");

  ltx->DrawLatex(0.15, gdir_label_ypos + 0.1, isolabel);
  ltx->DrawLatex(0.15, gdir_label_ypos + 0.05, etalabel);
  gPad->Update();
  idx = 0;
  if (splitImassShowShape)
  {
    draw_legend_m(0.07, gdir_leg_ypos, eff_h, "imass rej", idx++);
    draw_legend_m(0.07, gdir_leg_ypos, eff_shape_h, "show shape", idx++);
  }
  else
  {
    draw_legend_m(0.07, gdir_leg_ypos, eff_h, "dec rej (IM+SS)", idx++);
  }
  draw_legend_m(0.07, gdir_leg_ypos, eff_iso_h, "isolation", idx++);
  draw_legend_m(0.07, gdir_leg_ypos, eff_djiso_h, "dec rej + iso", idx++);

  TCanvas *c3 = new TCanvas("c3", "c3: direct/all", 600, 600);
  if (xsec_mb != 0 && xsec_dg != 0)
  { // Final ratio only if xsec scaling is used
    gPad->SetLogy();
    TH1D *gamma_rat_h = (TH1D *)ptclust_mb->Clone("gamma_rat_h");
    gamma_rat_h->Add(ptclust_dg);
    gamma_rat_h->Divide(ptclust_dgsel, gamma_rat_h, 1, 1, "B");
    gamma_rat_h->SetXTitle("#it{p}_{T} (GeV/#it{c})");
    gamma_rat_h->SetYTitle("#gamma_{dir}/all clusters");
    gamma_rat_h->SetMaximum(3);
    gamma_rat_h->SetMinimum(1e-3);
    gamma_rat_h->GetXaxis()->SetRangeUser(minPt, 15);
    gamma_rat_h->Draw();

    TH1D *gamma_rat_imass_h = 0, *gamma_rat_shape_h = 0, *gamma_rat_dj_h = 0;
    if (splitImassShowShape)
    {
      gamma_rat_imass_h = (TH1D *)ptcl_imassrej_mb->Clone("gamma_rat_imass_h");
      gamma_rat_imass_h->Add(ptcl_imassrej_dg);
      gamma_rat_imass_h->Divide(ptcl_imassrej_dg, gamma_rat_imass_h, 1, 1, "B");
      gamma_rat_imass_h->Draw("same");
      gamma_rat_shape_h = (TH1D *)ptcl_ssrej_mb->Clone("gamma_rat_shape_h");
      gamma_rat_shape_h->Add(ptcl_ssrej_dg);
      gamma_rat_shape_h->Divide(ptcl_ssrej_dg, gamma_rat_shape_h, 1, 1, "B");
      gamma_rat_shape_h->Draw("same");
    }
    else
    {
      gamma_rat_dj_h = (TH1D *)ptcl_decrej_mb->Clone("gamma_rat_dj_h");
      gamma_rat_dj_h->Add(ptcl_decrej_dg);
      gamma_rat_dj_h->Divide(ptcl_decrej_dg, gamma_rat_dj_h, 1, 1, "B");
      gamma_rat_dj_h->Draw("same");
    }

    TH1D *gamma_rat_iso_h = (TH1D *)ptcl_iso_mb->Clone("gamma_rat_iso_h");
    gamma_rat_iso_h->Add(ptcl_iso_dg);
    gamma_rat_iso_h->Divide(ptcl_iso_dg, gamma_rat_iso_h, 1, 1, "B");
    gamma_rat_iso_h->Draw("same");

    TH1D *gamma_rat_djiso_h = (TH1D *)ptcl_decrejiso_mb->Clone("gamma_rat_djiso_h");
    gamma_rat_djiso_h->SetMarkerColor(2);
    gamma_rat_djiso_h->SetLineColor(2);
    gamma_rat_djiso_h->Add(ptcl_decrejiso_dg);
    gamma_rat_djiso_h->Divide(ptcl_decrejiso_dg, gamma_rat_djiso_h, 1, 1, "B");
    gamma_rat_djiso_h->Draw("same");

    ltx->DrawLatex(0.13, 0.85, label1);
    ltx->DrawLatex(0.13, 0.81, etalabel);
    ltx->DrawLatex(0.13, 0.77, label2);

    if (alice_label)
    {
      ltx->DrawLatex(0.6, 0.85, "ALICE simulation");
      ltx->DrawLatex(0.63, 0.81, "FoCal upgrade");
    }
    ltx->DrawLatex(0.13, 0.15, isolabel);

    gPad->Update();
    idx = 0;
    draw_legend_m(0.6, 0.2, gamma_rat_djiso_h, "dec rej + iso", idx++);
    if (splitImassShowShape)
    {
      draw_legend_m(0.6, 0.2, gamma_rat_imass_h, "imass rej", idx++);
      draw_legend_m(0.6, 0.2, gamma_rat_shape_h, "show shape rej", idx++);
    }
    else
    {
      draw_legend_m(0.6, 0.2, gamma_rat_dj_h, "dec rej (IM+SS)", idx++);
    }

    draw_legend_m(0.6, 0.2, gamma_rat_iso_h, "isolation", idx++);
    draw_legend_m(0.6, 0.2, gamma_rat_h, "no selection", idx++);

    TCanvas *c4 = new TCanvas("c4", "c4: signal improvement", 600, 600);

    TH1D *sig_imp_rat = (TH1D *)gamma_rat_djiso_h->Clone("sig_imp_rat");
    sig_imp_rat->Divide(gamma_rat_h);
    sig_imp_rat->GetXaxis()->SetRangeUser(minPt, 15);
    sig_imp_rat->SetMaximum(30);
    sig_imp_rat->SetMinimum(0);
    sig_imp_rat->Draw();

    if (splitImassShowShape)
    {
      TH1D *sig_imp_rat_imass = (TH1D *)gamma_rat_imass_h->Clone("sig_imp_rat_imass");
      sig_imp_rat_imass->Divide(gamma_rat_h);
      sig_imp_rat_imass->Draw("same");
      TH1D *sig_imp_rat_shape = (TH1D *)gamma_rat_shape_h->Clone("sig_imp_rat_shape");
      sig_imp_rat_shape->Divide(gamma_rat_h);
      sig_imp_rat_shape->Draw("same");
    }
    else
    {
      TH1D *sig_imp_rat_imass = (TH1D *)gamma_rat_dj_h->Clone("sig_imp_rat_imass");
      sig_imp_rat_imass->Divide(gamma_rat_h);
      sig_imp_rat_imass->Draw("same");
    }

    TH1D *sig_imp_rat_iso = (TH1D *)gamma_rat_iso_h->Clone("sig_imp_rat_iso");
    sig_imp_rat_iso->Divide(gamma_rat_h);
    sig_imp_rat_iso->Draw("same");
  }
}
