/*
 *
 * Particle level analysis code for Pythia/Hijing
 *
 * Several QA histos are made, as well as tree that can be used
 * for plotting isolation energy etc
 *
 * Also provides options to 'cook' the MC in several ways, mostly
 *    - Add radial flow (betaFlow) to the particles in the isolation cone
 *    - Apply efficiency by random rejection (EMEff) to the particles in the isoltion cone
 *
 */
#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"
#include "TObjArray.h"
#include "TH1.h"
#include "TProfile.h"
#include "TRandom.h"
#include "TH2.h"
#include "TH3.h"
#include "TParticle.h"
#include "TSystem.h"
#include "Riostream.h"

#include "AliRunLoader.h"
#include "AliStack.h"
#include "AliFOCALCluster.h"

using std::cout;
using std::endl;

const Float_t ptMinTree = 4; // store only particles above ptMinTree
const Float_t betaFlow = 0;//0.6;  // Radial flow; use <= 0 for no flow
const Float_t EMEff = 0.4; //1.001; // Efficiency for EM energy deposits

void analyzePythiaStackOnlyAlgo(Int_t startFolder, Int_t endFolder, Int_t startEvent, Int_t endEvent, const Char_t* simFolder, const Char_t *clustersFolder, const Char_t* dataSampleTag, const Char_t *outputdir) {

  for (Int_t nfolder = startFolder; nfolder <= endFolder; nfolder++) {
    char filename[200];

    TFile *fout = new TFile(Form("%s/analysis_stackonly_betaFlow%03d_EMEff%03d_%s_%d.root",outputdir,Int_t(100*betaFlow),Int_t(100*EMEff),dataSampleTag,nfolder),"RECREATE");
    fout->cd();

    TProfile *xsec_h = new TProfile("xsec_h","Cross section",1,0,1);
    TH1D *trials_h = new TH1D("trials_h","Trials",1,0,1);
    TH1D *nevt_h = new TH1D("nevt_h","Number of Events",1,0,1);

    TTree * analysis = new TTree("analysis","track Info Tree");

    Int_t ievt, iTrk;
    Float_t eta, phi, pt, Epart;
    Int_t pdg, pdg_parent;
    Float_t ptisoR2Had, ptisoR2EM, ptisoR4Had, ptisoR4EM; // pt sums for isolation cut
    analysis->Branch("ievt",&ievt,"ievt/I");
    analysis->Branch("ipart",&iTrk,"ipart/I");
    analysis->Branch("eta",&eta,"eta/F");
    analysis->Branch("phi",&phi,"phi/F");
    analysis->Branch("pt",&pt,"pt/F");
    analysis->Branch("Epart",&Epart,"Epart/F");
    analysis->Branch("pdg",&pdg,"pdg/I");
    analysis->Branch("pdg_parent",&pdg_parent,"pdg_parent/I");

    analysis->Branch("ptisoR2HadMC",&ptisoR2Had,"ptisoR2HadMC/F");  
    analysis->Branch("ptisoR2EMMC",&ptisoR2EM,"ptisoR2EMMC/F");  
    analysis->Branch("ptisoR4HadMC",&ptisoR4Had,"ptisoR4HadMC/F");
    analysis->Branch("ptisoR4EMMC",&ptisoR4EM,"ptisoR4EMMC/F");

    TH2F *eta_pt_hadrons = new TH2F("eta_pt_hadrons","eta vs pt hadrons;#eta;p_{T} (GeV)",120,-6.0,6.0,75,0,7.5);
    TH2F *eta_pt_photons = new TH2F("eta_pt_photons","eta vs pt photons;#eta;p_{T} (GeV)",120,-6.0,6.0,75,0,7.5);
    TH2F *eta_pt_pizero = new TH2F("eta_pt_pizero","eta vs pt pizero;#eta;p_{T} (GeV)",120,-6.0,6.0,75,0,7.5);
    TH2F *eta_pt_pipm = new TH2F("eta_pt_pipm","eta vs pt #pi^{#pm};#eta;p_{T} (GeV)",120,-6.0,6.0,75,0,7.5);
    TH2F *eta_pt_p = new TH2F("eta_pt_p","eta vs pt proton;#eta;p_{T} (GeV)",120,-6.0,6.0,75,0,7.5);
    TH2F *eta_pt_n = new TH2F("eta_pt_n","eta vs pt neutron;#eta;p_{T} (GeV)",120,-6.0,6.0,75,0,7.5);
    TH2F *eta_pt_trigger = new TH2F("eta_pt_trigger","eta vs pt trigger particle;#eta;p_{T} (GeV)",20,2.5,4.5,75,0,7.5);

    TH2F *eta_pt_hadrons_flow = new TH2F("eta_pt_hadrons_flow","eta vs pt hadrons with flow;#eta;p_{T} (GeV)",40,2.0,6.0,75,0,7.5);
    /*
    // Would need to fill for each event and then average over events.
    TProfile *et_profile_hadrons = new TProfile("et_profile_hadrons","Et p        rofile hadrons;#eta;p_{T} (GeV)",120,-6.0,6.0);
    TProfile *et_profile_charged = new TProfile("et_profile_charged","Et p        rofile charged;#eta;p_{T} (GeV)",120,-6.0,6.0);
    TProfile *et_profile_pizero = new TProfile("et_profile_pizero","Et pro        file pizero;#eta;p_{T} (GeV)",120,-6.0,6.0);

    TH1F *et_sum_hadrons = new TH1F("et_sum_hadrons","Et sum hadrons;#eta;        p_{T} (GeV)",120,-6.0,6.0);
    et_sum_hadrons->Sumw2();
    TH1F *et_sum_charged = new TH1F("et_sum_charged","Et sum charged;#eta;        p_{T} (GeV)",120,-6.0,6.0);
    et_sum_charged->Sumw2();
    TH1F *et_sum_pizero = new TH1F("et_sum_pizero","Et sum pizero;#eta;p_{        T} (GeV)",120,-6.0,6.0);
    et_sum_pizero->Sumw2();
    */
    
    TFile *fin_xsec = new TFile(Form("%s/%d/pyxsec.root",simFolder,nfolder));
    if (!fin_xsec->IsOpen()) {
      cout << "No cross section info found" << endl;
    }
    if (fin_xsec->IsOpen()) {
    TTree *xc_tree = (TTree*) fin_xsec->Get("Xsection");

    Double_t xsec;
    UInt_t ntrials;
    xc_tree->SetBranchAddress("xsection",&xsec);
    xc_tree->SetBranchAddress("ntrials",&ntrials);
    xc_tree->GetEvent(0);
    cout << "folder " << nfolder << " trials " << ntrials << " xsec " << xsec << endl;
    xsec_h->Fill(0.5,xsec,ntrials);
    trials_h->Fill(0.5,ntrials);
    delete fin_xsec;
    }

    AliRunLoader *fRunLoader = 0;
    cout << "FOLDER: " << nfolder << endl;
	
    sprintf(filename,"%s/%i/%s",simFolder,nfolder,"galice.root");
    
    Long_t id = 0, size = 0, flags = 0, mt = 0;
    if (gSystem->GetPathInfo(filename,&id,&size,&flags,&mt) == 1) {
      cout << "ERROR: FOLDER: " << nfolder << endl;
      continue;      
    }

    //Alice run loader

    fRunLoader = AliRunLoader::Open(filename);
        
    if (!fRunLoader) {
      cout << "ERROR: FOLDER: " << nfolder << endl;
      continue;   
    }
    if (!fRunLoader->GetAliRun()) fRunLoader->LoadgAlice();
    if (!fRunLoader->TreeE()) fRunLoader->LoadHeader();
    if (!fRunLoader->TreeK()) fRunLoader->LoadKinematics();

    cout << "start loop" << endl;
    
    TObjArray primTracks;
    Int_t maxTrkFlow = 1000;
    Float_t *ptFlow = new Float_t[maxTrkFlow];
    Float_t *phiFlow = new Float_t[maxTrkFlow];
    
    for (ievt = startEvent; ievt <= endEvent; ievt++) {

      // Get MC Stack
      Int_t ie = fRunLoader->GetEvent(ievt);

      cout << "Event: " << ievt << " folder " << nfolder << " event " << ievt << endl;
      if (ie != 0)
         continue;
      nevt_h->Fill(0.5); // count events

      AliStack *stack = fRunLoader->Stack();

      Float_t pt_leading = -1;
      Float_t eta_leading = 0;

      // Loop over all tracks (needed for Hijing; Pythia store decay gammas in primaries...
      // Store selected tracks in the acceptance in primTracks and then Fill Tree in second loop
      primTracks.Clear();
      ptFlow[0]=-1; // The boosted particles are cached in these arrays; this is a flag that resets the cache for this event
      for (iTrk = 0; iTrk < stack->GetNtrack(); iTrk++) {
	TParticle *part = stack->Particle(iTrk);
	if (part->GetStatusCode() >= 10 && part->GetPdgCode() != 111) // Not stable (keep pi0)
	  continue;
        if (! stack->IsPhysicalPrimary(iTrk) && part->GetPdgCode() != 111) // select phys prim + pi0
          continue;

	if (part->GetPdgCode() == 111) {
	  eta_pt_pizero->Fill(part->Eta(), part->Pt());
	}
	else if (part->GetPdgCode() == 22) {
	  eta_pt_photons->Fill(part->Eta(), part->Pt());
	}
	else {
	  eta_pt_hadrons->Fill(part->Eta(), part->Pt());
	  if (TMath::Abs(part->GetPdgCode()) == 211) {
	    eta_pt_pipm->Fill(part->Eta(), part->Pt());
	  }
	  else if (part->GetPdgCode() == 2112) {
	    eta_pt_n->Fill(part->Eta(), part->Pt());
	  }
	  else if (part->GetPdgCode() == 2212) {
	    eta_pt_p->Fill(part->Eta(), part->Pt());
	  }
        }
	  /*
	    if (iTrk > stack->GetNprimary()) 
	    // && part->GetMother(0) > stack->GetNprimary()) 
	    // Check primary; pi0 decay in pythia, so gammas are from pythia...
	    continue;
	  */
	if (part->Eta() < 2.5 || part->Eta() > 6.0) 
	  continue;
        if (part->GetPdgCode() == 111)
 	  continue;

	primTracks.AddLast(part);
      }


      Int_t nPrimTrk = primTracks.GetEntries();
      // cout << "Primary tracks in FOCAL " << nPrimTrk << endl;

      for (iTrk = 0; iTrk < nPrimTrk; iTrk++) {
	TParticle *part = (TParticle*) primTracks[iTrk];
	if (part->Pt() < ptMinTree)
	  continue;

	eta = part->Eta();
	phi = part->Phi();
	pt = part->Pt();
	Epart = part->Energy();

	pdg = part->GetPdgCode();
        pdg_parent = -1;
        if (part->GetFirstMother() >= 0) {
	  TParticle *parent = stack->Particle(part->GetFirstMother());
          pdg_parent = parent->GetPdgCode();
	}

	ptisoR2Had = 0;
	ptisoR2EM = 0;
	ptisoR4Had = 0;
	ptisoR4EM = 0;
	
        if (ptFlow[0] < 0) {
          // Apply boost
          if (nPrimTrk > maxTrkFlow) {
            delete [] ptFlow;
            delete [] phiFlow;
            maxTrkFlow = nPrimTrk;
            ptFlow = new Float_t[maxTrkFlow];
            phiFlow = new Float_t[maxTrkFlow];
          }
          Float_t gammaFlow=TMath::Sqrt(1./(1.-betaFlow*betaFlow));
	  for (Int_t iTrk2 = 0; iTrk2 < nPrimTrk; iTrk2++) {
	     TParticle *part2 = (TParticle*) primTracks[iTrk2];
             if (betaFlow <= 0) { // No boost
               ptFlow[iTrk2] = part2->Pt();
               phiFlow[iTrk2] = part2->Phi();
             }
             else {
               TLorentzVector mom;
               Double_t Et = TMath::Sqrt(part2->Energy()*part2->Energy() - part2->Pz()*part2->Pz());
               mom.SetPxPyPzE(part2->Px(),part2->Py(),0,Et);
               Double_t phiFlowVec = gRandom->Rndm()*TMath::Pi()*2;
               TVector3 bflow(betaFlow*TMath::Cos(phiFlowVec),betaFlow*TMath::Sin(phiFlowVec),0);
               mom.Boost(bflow);
               ptFlow[iTrk2] = mom.Pt();
               phiFlow[iTrk2] = mom.Phi();
               //cout << "boosted particle: pt " << part2->Pt() << " -> " << ptFlow[iTrk2] << " phi " << part2->Phi() << " -> " << phiFlow[iTrk2] << " beta " << " gamma mass beta " << gammaFlow*part2->GetMass()*betaFlow << endl;
               if (TMath::Abs(part2->GetPdgCode()) != 11 && part2->GetPdgCode() != 22)
                 eta_pt_hadrons_flow->Fill(part2->Eta(), ptFlow[iTrk2]);
             }
          }
        }
        // cout << "Isolation energy for track " << iTrk << " pdg " << pdg << " pt " << pt << endl;
	for (Int_t iTrk2 = 0; iTrk2 < nPrimTrk; iTrk2++) {
          if (iTrk2 == iTrk)
             continue;
	  TParticle *part2 = (TParticle*) primTracks[iTrk2];

	  // first do eta-phi matching for isolation cut
	  Float_t deta = part2->Eta() - part->Eta();
	  if (TMath::Abs(deta) > 0.4)
	    continue;
	  Float_t dphi = phiFlow[iTrk2] - part->Phi();
	  
	  if (dphi < -TMath::Pi())
	    dphi += 2*TMath::Pi();
	  if (dphi > TMath::Pi())
	    dphi -= 2*TMath::Pi();
	  if (TMath::Abs(dphi) > 0.4)
	    continue;

	  Float_t R = TMath::Sqrt(deta*deta+dphi*dphi);
	  if (R < 0.4) {
	    if (part2->GetPdgCode() == 22 || TMath::Abs(part2->GetPdgCode()) == 11) {
	      if (EMEff >= 1 || gRandom->Rndm() < EMEff)
                 ptisoR4EM += ptFlow[iTrk2];
            }
	    else
	      ptisoR4Had += ptFlow[iTrk2];
	    if (R < 0.2) {
	      if (part2->GetPdgCode() == 22 || TMath::Abs(part2->GetPdgCode()) == 11) {
	        if (EMEff >= 1 || gRandom->Rndm() < EMEff)
	            ptisoR2EM += ptFlow[iTrk2];
              }
	      else
		ptisoR2Had += ptFlow[iTrk2];
	      
	    }
	  }
	}

	if (part->Pt() > pt_leading) {
	  pt_leading = part->Pt();
	  eta_leading = part->Eta();
	}

        analysis->Fill();
      }
      eta_pt_trigger->Fill(eta_leading, pt_leading);
    }

    fout->Write();
    fout->Close();
    fRunLoader->Delete();
  }
}

