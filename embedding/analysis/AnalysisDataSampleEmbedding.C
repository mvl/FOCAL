
    //START Data sample info
        
        // Will be included in the output file name
        
        // Tag of the digit files, this needs to be set according to the input files
        // The generic inputfile name format is: digits_[DIGITSTAG]_[NUMBER].root
        char clusteringTag[200] = "embedded_AA_pi09_loiv3";
        //char clusteringTag[200] = "narrow";
        
        // Folder with simulated data. 
        // For hits it is expected that folders numbered 
        //   [startFolder-endFolder] will be found inside.
        // For digits it is expected that digit files will be found inside 
        //   (see generic name format above)
        char clustersFolder[200] = "/home/focal/storage/clusters/embedding/AA5_pi09";
        
        // in which folder is MonteCarlo info stored
        char MCInfoTag[200] = "config_pi0_p1-400";
        char MCInfoDir[200] = "~/users/mvl/new_clusterizer2/framework_full_example/pi0/MCInfo/results";
        
        // Where output files should be stored
          char outputFileDir[200] = "./results";

//        // DONE NOW AS RUNCLUSTERIZER ARGUMENTS
//        // Which folders (or files in case of digits) and events should be analyzed
//        Int_t startFolder = 1;
//        Int_t endFolder = 2;
//        Int_t startEvent = 0;
//        Int_t endEvent = 1;
    
    // END Data sample information
  
